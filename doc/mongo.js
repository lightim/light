conn = new Mongo();
db = conn.getDB("light");
// 创建索引，这些索引非常重要

db.app.ensureIndex({
    appId: 1,
}, { unique: true });

/* 用户token */
db.token.ensureIndex({
    appId: 1,
    userId: 1,
    client: 1,
}, { unique: true });
db.token.ensureIndex({
    token: 1,
    client: 1,
});

/* 消息 */
db.message.ensureIndex({
    appId: 1,
    toUserId: 1,
    sent: 1,
    ctime: 1
});
db.message.ensureIndex({
    id: 1
});
db.message.ensureIndex({
    id: 1,
    type: 1,
});

/* 会话 */
db.session.ensureIndex({
    appId: 1,
    userId: 1,
    type: 1,
    targetId: 1,
});
db.session.ensureIndex({
    appId: 1,
    userId: 1,
    utime: 1,
});

/* 用户的好友关系 */
db.friend.ensureIndex({
    appId: 1,
    userId: 1,
    friendId: 1,
}, { unique: true });
db.friend.ensureIndex({
    appId: 1,
    userId: 1,
});

/* 群组关系 */
db.group_user.ensureIndex({
    appId: 1,
    groupId: 1,
});
db.group_user.ensureIndex({
    appId: 1,
    groupId: 1,
    userId: 1,
}, { unique: true });

/* 聊天室关系 */
db.room_user.ensureIndex({
    appId: 1,
    groupId: 1,
});
db.room_user.ensureIndex({
    appId: 1,
    groupId: 1,
    userId: 1,
}, { unique: true });
db.room_user.ensureIndex({
    appId: 1,
    userId: 1,
    temp: 1,
});
