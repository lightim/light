package errors

import (
	"fmt"
	"net/http"
)

/* 这里的错误全部都是对客户端的 */
var (
	ErrInternalException = NewError(http.StatusInternalServerError, -1, "服务器错误")
	ErrSourceInvalid     = NewError(http.StatusBadRequest, -2, "客户端未定义")
	ErrRequestInvalid    = NewError(http.StatusBadRequest, -3, "参数错误")

	ErrAppIdExists       = NewError(http.StatusConflict, 1001, "App Id冲突")
	ErrAppNotFound       = NewError(http.StatusNotFound, 1002, "App 不存在")
	ErrAppSecretIncorret = NewError(http.StatusForbidden, 1003, "App 密码错误")
)

func NewError(status, code int, msg interface{}) *Error {
	return &Error{
		HttpStatus: status,
		Status:     code,
		Message:    fmt.Sprintf("%v", msg),
	}
}

func CopyError(e *Error) *Error {
	err := new(Error)
	*err = *e
	return err
}

func CopyErrorWithMsg(e *Error, msg string) *Error {
	err := CopyError(e)
	err.Message = msg
	return err
}

type Error struct {
	HttpStatus int    `json:"-"`
	Status     int    `json:"status"`
	Message    string `json:"message"`
}

func (e *Error) Error() string {
	return e.Message
}
