package main

import (
	"context"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/sessions"
	"github.com/labstack/echo"
	"github.com/labstack/echo-contrib/session"
	"github.com/labstack/echo/middleware"
	"gitlab.com/lightim/light/cache"
	"gitlab.com/lightim/light/config"
	"gitlab.com/lightim/light/database"
	"gitlab.com/lightim/light/electron/controller"
	"gitlab.com/lightim/light/log"
)

const AppName = "electron"

func initConfig() {
	config.Init(AppName)
}

func initLog() {
	log.Init()
}

func initDatabase() {

	url := config.GetString("db.mongo.url")
	dbname := config.GetString("db.mongo.dbname")
	database.InitMongoDB(url, dbname)
}

func initCache() {
	addr := config.GetString("cache.redis.addr")
	password := config.GetString("cache.redis.password")
	db := config.GetInt("cache.redis.db")
	cache.Init(addr, password, db)
}

func main() {
	initConfig()
	initLog()
	initDatabase()
	initCache()

	runEcho(controller.Register)
}

/* 初始化HTTP服务 */
func runEcho(f func(*echo.Echo)) {
	httpAddr := config.GetString("http.listen")
	allowOrigins := config.GetStringSlice("http.cors.origins")
	allowMethods := config.GetStringSlice("http.cors.methods")
	allowCredentials := config.GetBool("http.cors.credentials")
	sessionSecret := config.GetString("http.session.secret")

	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.RequestID())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     allowOrigins,
		AllowHeaders:     []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept, "Source"},
		AllowCredentials: allowCredentials,
		AllowMethods:     allowMethods,
	}))
	e.Use(session.Middleware(sessions.NewCookieStore([]byte(sessionSecret))))
	e.HideBanner = true

	/* 注册路由等 */
	f(e)

	go func() {
		if err := e.Start(httpAddr); err != nil {
			log.Infof("shutting down %s", AppName)
		}
	}()
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt, os.Kill)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := e.Shutdown(ctx); err != nil {
		panic(err)
	}
}
