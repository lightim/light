package app

import (
	"github.com/labstack/echo"
	userdb "gitlab.com/lightim/light/database/user"
	"gitlab.com/lightim/light/electron/controller/context"
	"gitlab.com/lightim/light/electron/errors"
)

type CreateAppRequest struct {
	Name           string `json:"name" form:"name" query:"name" validate:"gt=0"`
	AllowAnonymous bool   `json:"allow_anonymous" form:"allow_anonymous" query:"allow_anonymous"`
	AllowStranger  bool   `json:"allow_stranger" form:"allow_stranger" query:"allow_stranger"`
}

func CreateApp(c echo.Context) error {
	ctx := c.(*context.Context)
	var req CreateAppRequest
	if err := ctx.Bind(&req); err != nil {
		return errors.ErrRequestInvalid
	} else if err := ctx.Validate(&req); err != nil {
		return errors.ErrRequestInvalid
	}

	name := req.Name
	allowAnonymous := req.AllowAnonymous
	allowStranger := req.AllowStranger

	app, err := userdb.CreateApp(name, allowAnonymous, allowStranger)
	if err != nil {
		return errors.ErrInternalException
	}
	return ctx.SUCCESS(app)
}
