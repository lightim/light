package controller

import (
	"net/http"

	"github.com/go-playground/validator"
	"github.com/labstack/echo"
	"gitlab.com/lightim/light/electron/controller/app"
	"gitlab.com/lightim/light/electron/controller/friend"
	"gitlab.com/lightim/light/electron/controller/group"
	"gitlab.com/lightim/light/electron/controller/message"
	"gitlab.com/lightim/light/electron/controller/middleware"
	"gitlab.com/lightim/light/electron/controller/room"
	"gitlab.com/lightim/light/electron/controller/session"
	"gitlab.com/lightim/light/electron/controller/token"
	"gitlab.com/lightim/light/electron/errors"
)

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}

func errorHandler(err error, c echo.Context) {
	if e, ok := err.(*errors.Error); ok {
		c.JSON(e.HttpStatus, e)
	} else if e, ok := err.(*echo.HTTPError); ok {
		c.JSON(e.Code, errors.NewError(0, -1, e.Message))
	} else {
		c.JSON(http.StatusInternalServerError, errors.NewError(0, -1, err.Error()))
	}
}

func Register(e *echo.Echo) {
	e.Validator = &CustomValidator{validator: validator.New()}
	e.HTTPErrorHandler = errorHandler
	api := e.Group("/api", middleware.ContextMiddleware)
	authApi := api.Group("", middleware.SecureMiddleware)

	api.POST("/app", app.CreateApp)

	authApi.POST("/user/token", token.CreateToken)
	authApi.POST("/user/friend", friend.AddFriend)
	authApi.DELETE("/user/friend", friend.DeleteFriend)
	authApi.GET("/user/sessions", session.FindUserSessions)

	authApi.POST("/group/users", group.AddGroupUsers)
	authApi.DELETE("/group/users", group.DeleteGroupUsers)

	authApi.POST("/room/users", room.AddRoomUsers)
	authApi.DELETE("/room/users", room.DeleteRoomUsers)

	authApi.POST("/message/notification", message.SendNotificationMessage)
	authApi.POST("/message/contentmessage", message.SendContentMessage)
}
