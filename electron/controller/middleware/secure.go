package middleware

import (
	"github.com/labstack/echo"
	userdb "gitlab.com/lightim/light/database/user"
	"gitlab.com/lightim/light/electron/controller/context"
	"gitlab.com/lightim/light/electron/errors"
)

func SecureMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.(*context.Context)
		appId := ctx.Request().Header.Get("X-APP")
		appSecret := ctx.Request().Header.Get("X-Secret")
		app, err := userdb.GetAppById(appId)
		if err != nil {
			return errors.ErrInternalException
		} else if app == nil {
			return errors.ErrAppNotFound
		} else if app.SecretKey != appSecret {
			return errors.ErrAppSecretIncorret
		}
		ctx.App = app
		return next(ctx)
	}
}
