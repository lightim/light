package session

import (
	"github.com/labstack/echo"
	sessiondb "gitlab.com/lightim/light/database/session"
	"gitlab.com/lightim/light/electron/controller/context"
	"gitlab.com/lightim/light/electron/errors"
)

func FindUserSessions(c echo.Context) error {
	ctx := c.(*context.Context)

	appId := ctx.App.AppId
	userId := ctx.QueryParam("user_id")

	sessions, err := sessiondb.FindUserSessions(appId, userId, 500)
	if err != nil {
		return errors.ErrInternalException
	}
	return ctx.SUCCESS(sessions)
}
