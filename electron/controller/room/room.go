package room

import (
	"fmt"

	"github.com/labstack/echo"
	roomdb "gitlab.com/lightim/light/database/room"
	"gitlab.com/lightim/light/electron/controller/context"
	"gitlab.com/lightim/light/electron/errors"
)

type AddRoomUsersRequest struct {
	RoomId  string   `json:"room_id" form:"room_id" query:"room_id" validate:"gt=0"`
	UserIds []string `json:"user_ids" form:"user_ids" query:"user_ids" validate:"gt=0,dive,gt=0"`
	Temp    bool     `json:"temp" form:"temp" query:"temp"`
}

func AddRoomUsers(c echo.Context) error {
	ctx := c.(*context.Context)
	var req AddRoomUsersRequest
	if err := ctx.Bind(&req); err != nil {
		return errors.ErrRequestInvalid
	} else if err := ctx.Validate(&req); err != nil {
		return errors.ErrRequestInvalid
	}

	appId := ctx.App.AppId
	roomId := req.RoomId
	userIds := req.UserIds
	temp := req.Temp

	if err := roomdb.CreateRoomUsers(appId, roomId, userIds, temp); err != nil {
		return errors.ErrInternalException
	}
	return ctx.SUCCESS(nil)
}

type DeleteRoomUserRequest struct {
	RoomId  string   `json:"room_id" form:"room_id" query:"room_id" validate:"gt=0"`
	UserIds []string `json:"user_ids" form:"user_ids" query:"user_ids" validate:"gt=0,dive,gt=0"`
}

func DeleteRoomUsers(c echo.Context) error {
	ctx := c.(*context.Context)
	var req DeleteRoomUserRequest
	if err := ctx.Bind(&req); err != nil {
		return errors.ErrRequestInvalid
	} else if err := ctx.Validate(&req); err != nil {
		return errors.ErrRequestInvalid
	}

	fmt.Printf("%v\n", req.UserIds)

	appId := ctx.App.AppId
	roomId := req.RoomId
	userIds := req.UserIds

	if err := roomdb.DeleteRoomUsers(appId, roomId, userIds); err != nil {
		return errors.ErrInternalException
	}
	return ctx.SUCCESS(nil)
}
