package message

import (
	"time"

	"encoding/json"
	"github.com/labstack/echo"
	messagedb "gitlab.com/lightim/light/database/message"
	"gitlab.com/lightim/light/electron/controller/context"
	"gitlab.com/lightim/light/electron/errors"
	"gitlab.com/lightim/light/neutron/rpcall"
	"gitlab.com/lightim/light/proto"
	"gitlab.com/lightim/light/x"
)

/* 发送消息 */

func SendContentMessage(c echo.Context) error {
	ctx := c.(*context.Context)

	var req SendContentMessageRequest
	if err := ctx.Bind(&req); err != nil {
		return errors.ErrRequestInvalid
	} else if err := ctx.Validate(&req); err != nil {
		return errors.ErrRequestInvalid
	}

	appId := ctx.App.AppId
	fromUserId := req.FromUserId
	toUserId := req.ToUserId
	ntype := req.Type
	content, err := json.Marshal(req.Content)
	if err != nil {
		return errors.ErrRequestInvalid
	}

	m := &proto.Message{
		Id:         x.UUID(),
		Version:    proto.Version_V1,
		AppId:      appId,
		FromUserId: fromUserId,
		ToUserId:   toUserId,
		Type:       proto.Message_Type(ntype),
		Ctime:      time.Now().UnixNano(),
		Content:    string(content),
	}

	if err := messagedb.InsertContentMessage(m, false); err != nil {
		return errors.ErrInternalException
	}

	rpcall.SendMessage(appId, toUserId, m)

	return ctx.SUCCESS(m)
}

func SendNotificationMessage(c echo.Context) error {
	ctx := c.(*context.Context)
	var req SendNotificationMessageRequest

	if err := ctx.Bind(&req); err != nil {
		return errors.ErrRequestInvalid
	} else if err := ctx.Validate(&req); err != nil {
		return errors.ErrRequestInvalid
	}

	appId := ctx.App.AppId
	userId := req.UserId
	content, err := json.Marshal(req.Content)
	if err != nil {
		return errors.ErrRequestInvalid
	}

	needAck := req.NeedAck

	m := &proto.Message{
		Id:         x.UUID(),
		Version:    proto.Version_V1,
		AppId:      appId,
		FromUserId: "",
		ToUserId:   userId,
		Type:       proto.Message_Notification,
		Ctime:      time.Now().UnixNano(),
		Content:    string(content),
	}

	if err := messagedb.InsertNotification(m, false); err != nil {
		return errors.ErrInternalException
	}

	if rpcall.SendMessage(appId, userId, m) && !needAck {
		messagedb.UpdateMessageSent(m.Id, true)
	}

	return ctx.SUCCESS(m)
}
