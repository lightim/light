package message

type SendContentMessageRequest struct {
	FromUserId string            `json:"from_user_id" form:"from_user_id" query:"from_user_id" validate:"gt=0"`
	ToUserId   string            `json:"to_user_id" form:"to_user_id" query:"to_user_id" validate:"gt=0"`
	Type       int               `json:"type" form:"type" query:"type" validate:"gt=0"`
	Content    map[string]string `json:"content" form:"content" query:"content"`
}

type SendNotificationMessageRequest struct {
	UserId  string            `json:"user_id" form:"user_id" query:"user_id" validate:"gt=0"`
	Content map[string]string `json:"content" form:"content" query:"content"`
	NeedAck bool              `json:"need_ack" form:"need_ack" query:"need_ack"`
}
