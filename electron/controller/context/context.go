package context

import (
	"net/http"
	"strconv"
)

func (ctx *Context) SUCCESS(v interface{}) error {
	return ctx.JSON(http.StatusOK, map[string]interface{}{
		"status":  0,
		"message": "OK",
		"data":    v,
	})
}

func (ctx *Context) QueryInt(name string) int64 {
	v, err := strconv.Atoi(ctx.QueryParam(name))
	if err != nil {
		return 0
	}
	return int64(v)
}

func (ctx *Context) ParamInt(name string) int64 {
	v, err := strconv.Atoi(ctx.Param(name))
	if err != nil {
		return 0
	}
	return int64(v)
}
