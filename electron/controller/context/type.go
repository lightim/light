package context

import (
	"github.com/labstack/echo"
	userdb "gitlab.com/lightim/light/database/user"
)

type Context struct {
	echo.Context
	App *userdb.App
}
