package group

import (
	"github.com/labstack/echo"
	groupdb "gitlab.com/lightim/light/database/group"
	"gitlab.com/lightim/light/electron/controller/context"
	"gitlab.com/lightim/light/electron/errors"
)

type AddGroupUsersRequest struct {
	GroupId string   `json:"group_id" form:"group_id" query:"group_id" validate:"gt=0"`
	UserIds []string `json:"user_ids" form:"user_ids" query:"user_ids" validate:"gt=0,dive,gt=0"`
}

func AddGroupUsers(c echo.Context) error {
	ctx := c.(*context.Context)
	var req AddGroupUsersRequest
	if err := ctx.Bind(&req); err != nil {
		return errors.ErrRequestInvalid
	} else if err := ctx.Validate(&req); err != nil {
		return errors.ErrRequestInvalid
	}

	appId := ctx.App.AppId
	groupId := req.GroupId
	userIds := req.UserIds

	if err := groupdb.CreateGroupUsers(appId, groupId, userIds); err != nil {
		return errors.ErrInternalException
	}
	return ctx.SUCCESS(nil)
}

type DeleteGroupUsersRequest struct {
	GroupId string   `json:"group_id" form:"group_id" query:"group_id" validate:"gt=0"`
	UserIds []string `json:"user_ids" form:"user_ids" query:"user_ids" validate:"gt=0,dive,gt=0"`
}

func DeleteGroupUsers(c echo.Context) error {
	ctx := c.(*context.Context)
	var req DeleteGroupUsersRequest
	if err := ctx.Bind(&req); err != nil {
		return errors.ErrRequestInvalid
	} else if err := ctx.Validate(&req); err != nil {
		return errors.ErrRequestInvalid
	}

	appId := ctx.App.AppId
	groupId := req.GroupId
	userIds := req.UserIds

	err := groupdb.DeleteGroupUsers(appId, groupId, userIds)
	if err != nil {
		return errors.ErrInternalException
	}
	return ctx.SUCCESS(nil)
}
