package config

import (
	"flag"
	"fmt"
	"os"

	"github.com/spf13/viper"
)

/*
 * 初始化配置
 */
func Init(appName string) error {
	var err error
	var c string
	flag.StringVar(&c, "c", "", "config")
	flag.Parse()

	if c == "" {
		err = readInConfig(appName)
	} else {
		err = readInFile(c)
	}
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s", err))
	}
	return nil
}

func readInFile(c string) error {
	f, err := os.Open(c)
	if err != nil {
		return err
	}
	return viper.ReadConfig(f)
}

func readInConfig(appName string) error {
	viper.SetConfigName(appName)
	viper.AddConfigPath("/etc/light")
	viper.AddConfigPath(".")
	return viper.ReadInConfig()
}

func GetString(key string) string {
	return viper.GetString(key)
}

func GetInt(key string) int {
	return viper.GetInt(key)
}

func GetStringSlice(key string) []string {
	return viper.GetStringSlice(key)
}

func GetBool(key string) bool {
	return viper.GetBool(key)
}
