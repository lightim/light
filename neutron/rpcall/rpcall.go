package rpcall

import (
	"gitlab.com/lightim/light/log"
	"gitlab.com/lightim/light/proto"
	"gitlab.com/lightim/light/proton/service"
	"gitlab.com/lightim/light/session"
)

/* 将m发送给appid.userId */
func SendMessage(appId, userId string, m *proto.Message) bool {
	saddr := session.GetSession(appId, userId)
	ok := false
	if saddr != "" {
		ok = SendMessageTo(appId, userId, saddr, m)
	} else {
		log.Infof("%s.%s is offline", appId, userId)
	}
	if !ok {
		PushMessage(m)
	}
	return ok
}

func SendMessageTo(appId, userId, saddr string, m *proto.Message) bool {
	ok := false
	status, err := service.SendMessage(saddr, appId, userId, m)
	if err != nil {
		log.Warnf("SendMessage error:%v", err)
	} else if status != service.SendMessageReply_OK {
		log.Errorf("SendMessage failed:%v", status)
	} else {
		ok = true
	}
	return ok
}
