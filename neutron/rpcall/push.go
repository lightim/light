package rpcall

import (
	"gitlab.com/lightim/light/proto"
	"gitlab.com/lightim/light/queue"
)

var (
	pushEnabled = false
	pushFilter  map[proto.Message_Type]bool
)

func Init(enabled bool, filter []string) {
	pushEnabled = enabled
	if !pushEnabled {
		return
	}
	queue.InitPushQueue()
	pushFilter = make(map[proto.Message_Type]bool)
	for _, f := range filter {
		switch f {
		case "person":
			pushFilter[proto.Message_ContentMessage] = true
		case "group":
			pushFilter[proto.Message_GroupContentMessage] = true
		case "room":
			pushFilter[proto.Message_RoomContentMessage] = true
		}
	}
}

func PushMessage(m *proto.Message) {
	if !pushEnabled {
		return
	} else if !pushFilter[m.Type] {
		return
	}
	queue.PubNotification(m)
}
