package message

type MessageACK struct {
	Status int    `json:"status"`
	MsgId  string `json:"msg_id"`
}
