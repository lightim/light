package message

import (
	gproto "github.com/golang/protobuf/proto"
	"gitlab.com/lightim/light/log"
	"gitlab.com/lightim/light/proto"
	"gitlab.com/lightim/light/queue"
)

func Run(appName string) {
	notifies, err := queue.MessageQueue.Channel.Consume(queue.MessageQueue.QueueName, appName, true, false, true, false, nil)
	if err != nil {
		panic(err)
	}

	for {
		for notify := range notifies {
			var m proto.Message
			if err := gproto.Unmarshal(notify.Body, &m); err != nil {
				log.Errorf("Unmarshal error:%v", err)
				continue
			}
			switch m.Type {
			case proto.Message_ContentMessage:
				go handleContentMessage(&m)
			case proto.Message_GroupContentMessage:
				go handleGroupContentMessage(&m)
			case proto.Message_RoomContentMessage:
				go handleRoomContentMessage(&m)
			case proto.Message_MessageAck:
				go handleMessageAck(&m)
			}
		}
	}
}
