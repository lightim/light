package message

import (
	"time"

	"encoding/json"

	frienddb "gitlab.com/lightim/light/database/friend"
	groupdb "gitlab.com/lightim/light/database/group"
	messagedb "gitlab.com/lightim/light/database/message"
	roomdb "gitlab.com/lightim/light/database/room"
	sessiondb "gitlab.com/lightim/light/database/session"
	userdb "gitlab.com/lightim/light/database/user"
	"gitlab.com/lightim/light/log"
	"gitlab.com/lightim/light/neutron/rpcall"
	"gitlab.com/lightim/light/proto"
	"gitlab.com/lightim/light/x"
)

/* 处理私聊消息 */
func handleContentMessage(m *proto.Message) {
	log.Infof("ContentMessage: %v", m.Id)
	if m.Content == "" {
		return
	}
	appId := m.AppId
	fromUserId := m.FromUserId
	toUserId := m.ToUserId

	ack := MessageACK{
		Status: 1,
		MsgId:  m.Id,
	}

	pm := &proto.Message{
		Version:    m.Version,
		Id:         x.UUID(),
		AppId:      appId,
		FromUserId: "",
		ToUserId:   fromUserId,
		Type:       proto.Message_MessageAck,
		Ctime:      time.Now().UnixNano(),
	}

	go sessiondb.UpsertSession(appId, fromUserId, sessiondb.STypePerson, toUserId)

	allowStranger := false
	app, _ := userdb.GetAppById(appId)
	if app != nil { /* 除非数据库或者缓存出问题，否则一定会有app */
		allowStranger = app.AllowStranger
	}

	if !allowStranger { /* 不允许陌生人发送消息，则检查是否加为好友 */
		friend, err := frienddb.GetFriendByUFId(appId, toUserId, fromUserId)
		if err != nil || friend == nil {
			content, _ := json.Marshal(ack)
			pm.Content = string(content)
			ok := rpcall.SendMessage(appId, fromUserId, pm)
			messagedb.InsertContentMessageAck(pm, ok)
			return
		}
	}

	go func() {
		messagedb.InsertContentMessage(m, false)
		rpcall.SendMessage(appId, toUserId, m)
	}()

	ack.Status = 0
	content, _ := json.Marshal(ack)
	pm.Content = string(content)
	ok := rpcall.SendMessage(appId, fromUserId, pm)
	messagedb.InsertContentMessageAck(pm, ok)
}

/* 处理群消息 */
func handleGroupContentMessage(m *proto.Message) {
	log.Infof("GroupContentMessage: %v", m.Id)
	if m.Content == "" {
		return
	}

	appId := m.AppId
	fromUserId := m.FromUserId
	toGroupId := m.ToUserId

	ack := MessageACK{
		Status: 1,
		MsgId:  m.Id,
	}

	pm := &proto.Message{
		Version:    m.Version,
		Id:         x.UUID(),
		AppId:      appId,
		FromUserId: "",
		ToUserId:   fromUserId,
		Type:       proto.Message_MessageAck,
		Ctime:      time.Now().UnixNano(),
	}

	gu, err := groupdb.GetGroupUserByGUId(appId, toGroupId, fromUserId)
	if err != nil {
		return
	} else if gu == nil { /* 发送方并不在群里 */
		content, _ := json.Marshal(ack)
		pm.Content = string(content)
		ok := rpcall.SendMessage(appId, fromUserId, pm)
		messagedb.InsertContentMessageAck(pm, ok)
		return
	}

	go sessiondb.UpsertSession(appId, fromUserId, sessiondb.STypeGroup, toGroupId)

	users, err := groupdb.FindGroupUsers(appId, toGroupId)
	if err != nil {
		return
	}

	/* 群消息保存一份 */
	m.ToUserId = ""
	if err := messagedb.InsertContentMessage(m, true); err != nil {
		return
	}

	for _, u := range users {
		if u.UserId == fromUserId {
			continue
		}
		pm := *m /* Copy */
		go func(appId, userId string, m *proto.Message) {
			m.Id = x.UUID()
			m.ToUserId = userId
			messagedb.InsertContentMessage(m, false)
			rpcall.SendMessage(appId, userId, m)
		}(appId, u.UserId, &pm)
	}
	ack.Status = 0
	content, _ := json.Marshal(ack)
	pm.Content = string(content)
	ok := rpcall.SendMessage(appId, fromUserId, pm)
	messagedb.InsertContentMessageAck(pm, ok)
}

/* 处理聊天室消息 */
func handleRoomContentMessage(m *proto.Message) {
	log.Infof("RoomContentMessage: %v", m.Id)
	if m.Content == "" {
		return
	}
	appId := m.AppId
	fromUserId := m.FromUserId
	ToRoomId := m.ToUserId

	ack := MessageACK{
		Status: 1,
		MsgId:  m.Id,
	}

	pm := &proto.Message{
		Version:    m.Version,
		Id:         x.UUID(),
		AppId:      appId,
		FromUserId: "",
		ToUserId:   fromUserId,
		Type:       proto.Message_MessageAck,
		Ctime:      time.Now().UnixNano(),
	}

	gu, err := roomdb.GetRoomUserByRUId(appId, ToRoomId, fromUserId)
	if err != nil {
		return
	} else if gu == nil { /* 发送方并不在聊天室里 */
		content, _ := json.Marshal(ack)
		pm.Content = string(content)
		ok := rpcall.SendMessage(appId, fromUserId, pm)
		messagedb.InsertContentMessageAck(pm, ok)
		return
	}

	go sessiondb.UpsertSession(appId, fromUserId, sessiondb.STypeRoom, ToRoomId)

	users, err := roomdb.FindRoomUsers(appId, ToRoomId)
	if err != nil {
		return
	}

	/* 聊天室消息只保存一份 */
	m.ToUserId = ""
	if err := messagedb.InsertContentMessage(m, true); err != nil {
		return
	}

	for _, u := range users {
		if u.UserId == fromUserId {
			continue
		}
		pm := *m /* Copy */
		go func(appId, userId string, m *proto.Message) {
			m.Id = x.UUID()
			m.ToUserId = userId
			rpcall.SendMessage(appId, userId, m)
		}(appId, u.UserId, &pm)
	}

	ack.Status = 0
	content, _ := json.Marshal(ack)
	pm.Content = string(content)
	ok := rpcall.SendMessage(appId, fromUserId, pm)
	messagedb.InsertContentMessageAck(pm, ok)
}

/* 处理聊天消息的相应 */
func handleMessageAck(m *proto.Message) {
	log.Infof("MessageAck: %v", m.Id)
	var ack MessageACK
	if err := json.Unmarshal([]byte(m.Content), &ack); err != nil {
		return
	}

	appId := m.AppId
	fromUserId := m.FromUserId

	if mm, err := messagedb.GetMessageById(ack.MsgId); err != nil {
		return
	} else if mm == nil {
		return
	} else {
		messagedb.UpdateMessageSent(mm.Id, true)

		if mm.Type == int(proto.Message_ContentMessage) {
			sessiondb.UpsertSession(appId, fromUserId, sessiondb.STypePerson, mm.FromUserId)
		} else if mm.Type == int(proto.Message_GroupContentMessage) {
			sessiondb.UpsertSession(appId, fromUserId, sessiondb.STypeGroup, mm.ToUserId)
		}
	}
}
