package event

import (
	"encoding/json"
	"time"

	messagedb "gitlab.com/lightim/light/database/message"
	roomdb "gitlab.com/lightim/light/database/room"
	"gitlab.com/lightim/light/log"
	"gitlab.com/lightim/light/neutron/rpcall"
	"gitlab.com/lightim/light/proto"
	"gitlab.com/lightim/light/queue"
)

func Run(appName string) {
	notifies, err := queue.EventQueue.Channel.Consume(queue.EventQueue.QueueName, appName, true, false, true, false, nil)
	if err != nil {
		panic(err)
	}

	for {
		for notify := range notifies {
			var e queue.Event
			if err := json.Unmarshal(notify.Body, &e); err != nil {
				log.Errorf("Unmarshal error:%v", err)
				continue
			}
			appId := e.AppId
			userId := e.UserId
			addr := e.Addr
			switch e.Type {
			case queue.EventTypeConnect:
				go handleUserConnect(appId, userId, addr)
			case queue.EventTypeDisconnect:
				go handleUserDisconnect(appId, userId, addr)
			}
		}
	}
}

func handleUserConnect(appId, userId, saddr string) {
	log.Debugf("%s.%s connected from %s", appId, userId, saddr)
	messages, _ := messagedb.FindMessageUnsent(appId, userId, time.Now().AddDate(0, -1, 0))
	for _, message := range messages {
		m := &proto.Message{
			Version:    proto.Version(message.Version),
			Id:         message.Id,
			FromUserId: message.FromUserId,
			ToUserId:   message.ToUserId,
			Type:       proto.Message_Type(message.Type),
			Ctime:      message.CTime.UnixNano(),
		}
		needAck := true
		switch message.Type {
		case int(proto.Message_ContentMessage), int(proto.Message_GroupContentMessage), int(proto.Message_RoomContentMessage), int(proto.Message_MessageAck):
			content, err := json.Marshal(message.Content)
			if err != nil {
				break
			}
			m.Content = string(content)
		case int(proto.Message_Notification):
			content, err := json.Marshal(message.Content)
			if err != nil {
				break
			}
			m.Content = string(content)
			needAck = message.NeedAck
		default:
			continue
		}
		if ok := rpcall.SendMessageTo(appId, userId, saddr, m); !ok {
			return
		} else if !needAck { /* 不需要ACK且已经发送成功 */
			messagedb.UpdateMessageSent(message.Id, true)
		}
	}
}

func handleUserDisconnect(appId, userId, saddr string) {
	roomdb.DeleteTempRoomUsers(appId, userId)
}
