package main

import (
	"os"
	"os/signal"

	"gitlab.com/lightim/light/cache"
	"gitlab.com/lightim/light/config"
	"gitlab.com/lightim/light/database"
	"gitlab.com/lightim/light/log"
	"gitlab.com/lightim/light/neutron/event"
	"gitlab.com/lightim/light/neutron/message"
	"gitlab.com/lightim/light/neutron/rpcall"
	"gitlab.com/lightim/light/queue"
)

const (
	AppName = "neutron"
)

func init() {
	initConfig()
	initLog()
	initCache()
	initDatabase()
	initQueue()
	initRPC()
}

func initConfig() {
	config.Init(AppName)
}

func initLog() {
	log.Init()
}

func initQueue() {
	url := config.GetString("queue.rabbitmq.url")
	queue.InitConn(url)
	queue.InitEventQueue()
	queue.InitMessageQueue()
}

func initRPC() {
	pushEnabled := config.GetBool("push.enabled")
	pushFilter := config.GetStringSlice("push.filter")
	rpcall.Init(pushEnabled, pushFilter)
}

func initCache() {
	addr := config.GetString("cache.redis.addr")
	password := config.GetString("cache.redis.password")
	db := config.GetInt("cache.redis.db")
	cache.Init(addr, password, db)
}

func initDatabase() {
	url := config.GetString("db.mongo.url")
	dbname := config.GetString("db.mongo.dbname")
	database.InitMongoDB(url, dbname)
}
func main() {
	go message.Run(AppName)
	go event.Run(AppName)

	log.Infof("%s started", AppName)

	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, os.Kill)
	<-c
}
