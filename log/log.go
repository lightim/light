package log

import (
	"os"

	"github.com/sirupsen/logrus"
)

func Init() {
	filenameHook := NewHook()
	filenameHook.Field = "file" // Customize source field name
	filenameHook.Skip = 5
	logrus.AddHook(filenameHook)
	logrus.SetFormatter(&logrus.JSONFormatter{})
	logrus.SetOutput(os.Stdout)
	logrus.SetLevel(logrus.DebugLevel)
}

func Errorf(format string, args ...interface{}) {
	logrus.Errorf(format, args...)
}

func Infof(format string, args ...interface{}) {
	logrus.Infof(format, args...)
}

func Debugf(format string, args ...interface{}) {
	logrus.Debugf(format, args...)
}

func Warnf(format string, args ...interface{}) {
	logrus.Warnf(format, args...)
}
