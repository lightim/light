

.PHONY: proto proton neutron electron sdk


all: proton neutron electron

proto:
	make -C proto proto

proton:
	make -C proton proton

neutron:
	make -C neutron neutron

electron:
	make -C electron electron

sdk:
	protoc -I=./proto --dart_out=sdk/dart ./proto/message.proto