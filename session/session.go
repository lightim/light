package session

import (
	"fmt"
	"time"

	"gitlab.com/lightim/light/cache"
)

const (
	SessionPrefix = "light.session"
)

func SetSession(appid, uid string, addr string, t time.Duration) error {
	key := fmt.Sprintf("%s.%s.%s", SessionPrefix, appid, uid)
	return cache.Set(key, addr, t)
}

func DelSession(appid, uid string) error {
	key := fmt.Sprintf("%s.%s.%s", SessionPrefix, appid, uid)
	return cache.Del(key)
}

func GetSession(appid, uid string) string {
	key := fmt.Sprintf("%s.%s.%s", SessionPrefix, appid, uid)
	v, _ := cache.Get(key)
	return v
}
