### proton

[proton](#proton)接收客户端连接并验证用户。一旦验证失败则关闭链接，验证成功则讲用户的链接信息写入[session](#session)。

[proton](#proton)会对收到的消息做简单的验证，比如某些必要字段是否存在，然后将消息写入[消息队列](#消息队列)。

### neutron

[neutron](#neutron)实现消息转发逻辑，当其确定了某条消息（私聊/群聊/控制消息等）的发送目标时，先从[session](#session)中检查用户在线状态，
如果用户在线，则通过[session](#session)中记录的链接信息，调用[proton](#proton)的RPC接口发送。

### electron

第三方集成的API接口，接口文档在**doc/electron.yml**。可将其复制到[Swagger](https://editor.swagger.io)查看。

### session

session用于保存在线用户的信息，目前的实现使用[redis](https://redis.io/)存储。

### 消息队列

消息队列用于传递消息，当[proton](#proton)收到有效的消息时，将其写入消息队列；再由[neutron](#neutron)消费处理。

目前的实现使用[rabbitmq](https://www.rabbitmq.com)。

### 数据存储

使用[MongoDB](https://www.mongodb.com)作为数据库，**doc/mongo.js**为建立数据表索引的脚本。

### SDK

目前的客户端SDK只有[Dart](https://www.dartlang.org)版本，可用于[Flutter](https://flutter.io/)开发。