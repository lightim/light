///
//  Generated code. Do not modify.
//  source: message.proto
///
// ignore_for_file: non_constant_identifier_names,library_prefixes,unused_import

const RequestType$json = const {
  '1': 'RequestType',
  '2': const [
    const {'1': 'Auth', '2': 0},
  ],
};

const Version$json = const {
  '1': 'Version',
  '2': const [
    const {'1': 'Undefined', '2': 0},
    const {'1': 'V1', '2': 1},
  ],
};

const Ping$json = const {
  '1': 'Ping',
  '2': const [
    const {'1': 'seq', '3': 1, '4': 1, '5': 3, '10': 'seq'},
  ],
};

const Pong$json = const {
  '1': 'Pong',
  '2': const [
    const {'1': 'seq', '3': 2, '4': 1, '5': 3, '10': 'seq'},
  ],
};

const AuthRequest$json = const {
  '1': 'AuthRequest',
  '2': const [
    const {'1': 'token', '3': 2, '4': 1, '5': 9, '10': 'token'},
    const {'1': 'client', '3': 3, '4': 1, '5': 9, '10': 'client'},
  ],
};

const Request$json = const {
  '1': 'Request',
  '2': const [
    const {'1': 'type', '3': 1, '4': 1, '5': 14, '6': '.proto.RequestType', '10': 'type'},
    const {'1': 'auth', '3': 2, '4': 1, '5': 11, '6': '.proto.AuthRequest', '10': 'auth'},
  ],
};

const AuthResponse$json = const {
  '1': 'AuthResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 14, '6': '.proto.AuthResponse.Status', '10': 'status'},
    const {'1': 'userId', '3': 2, '4': 1, '5': 9, '10': 'userId'},
  ],
  '4': const [AuthResponse_Status$json],
};

const AuthResponse_Status$json = const {
  '1': 'Status',
  '2': const [
    const {'1': 'Undefined', '2': 0},
    const {'1': 'OK', '2': 1},
    const {'1': 'AppNotFound', '2': 2},
    const {'1': 'TokenNotFound', '2': 3},
  ],
};

const Response$json = const {
  '1': 'Response',
  '2': const [
    const {'1': 'type', '3': 1, '4': 1, '5': 14, '6': '.proto.RequestType', '10': 'type'},
    const {'1': 'auth', '3': 2, '4': 1, '5': 11, '6': '.proto.AuthResponse', '10': 'auth'},
  ],
};

const ContentMessage$json = const {
  '1': 'ContentMessage',
  '2': const [
    const {'1': 'type', '3': 1, '4': 1, '5': 14, '6': '.proto.ContentMessage.Type', '10': 'type'},
    const {'1': 'content', '3': 4, '4': 3, '5': 11, '6': '.proto.ContentMessage.ContentEntry', '10': 'content'},
  ],
  '3': const [ContentMessage_ContentEntry$json],
  '4': const [ContentMessage_Type$json],
};

const ContentMessage_ContentEntry$json = const {
  '1': 'ContentEntry',
  '2': const [
    const {'1': 'key', '3': 1, '4': 1, '5': 9, '10': 'key'},
    const {'1': 'value', '3': 2, '4': 1, '5': 9, '10': 'value'},
  ],
  '7': const {'7': true},
};

const ContentMessage_Type$json = const {
  '1': 'Type',
  '2': const [
    const {'1': 'Undefined', '2': 0},
    const {'1': 'Text', '2': 1},
    const {'1': 'Image', '2': 2},
    const {'1': 'Voice', '2': 3},
    const {'1': 'Video', '2': 4},
    const {'1': 'File', '2': 5},
  ],
};

const MessageAck$json = const {
  '1': 'MessageAck',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 14, '6': '.proto.MessageAck.Status', '10': 'status'},
    const {'1': 'msgId', '3': 2, '4': 1, '5': 9, '10': 'msgId'},
  ],
  '4': const [MessageAck_Status$json],
};

const MessageAck_Status$json = const {
  '1': 'Status',
  '2': const [
    const {'1': 'Undefined', '2': 0},
    const {'1': 'OK', '2': 1},
    const {'1': 'BlackList', '2': 2},
    const {'1': 'NotFriend', '2': 3},
    const {'1': 'NotInGroup', '2': 4},
    const {'1': 'NotInRoom', '2': 5},
  ],
};

const Notification$json = const {
  '1': 'Notification',
  '2': const [
    const {'1': 'type', '3': 1, '4': 1, '5': 14, '6': '.proto.Notification.Type', '10': 'type'},
    const {'1': 'content', '3': 2, '4': 3, '5': 11, '6': '.proto.Notification.ContentEntry', '10': 'content'},
    const {'1': 'needAck', '3': 3, '4': 1, '5': 8, '10': 'needAck'},
  ],
  '3': const [Notification_ContentEntry$json],
  '4': const [Notification_Type$json],
};

const Notification_ContentEntry$json = const {
  '1': 'ContentEntry',
  '2': const [
    const {'1': 'key', '3': 1, '4': 1, '5': 9, '10': 'key'},
    const {'1': 'value', '3': 2, '4': 1, '5': 9, '10': 'value'},
  ],
  '7': const {'7': true},
};

const Notification_Type$json = const {
  '1': 'Type',
  '2': const [
    const {'1': 'Undefined', '2': 0},
    const {'1': 'KickOff', '2': 1},
    const {'1': 'FriendRequest', '2': 2},
    const {'1': 'FriendReply', '2': 3},
  ],
};

const Message$json = const {
  '1': 'Message',
  '2': const [
    const {'1': 'version', '3': 1, '4': 1, '5': 14, '6': '.proto.Version', '10': 'version'},
    const {'1': 'id', '3': 2, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'appId', '3': 3, '4': 1, '5': 9, '10': 'appId'},
    const {'1': 'fromUserId', '3': 4, '4': 1, '5': 9, '10': 'fromUserId'},
    const {'1': 'toUserId', '3': 5, '4': 1, '5': 9, '10': 'toUserId'},
    const {'1': 'toGroupId', '3': 6, '4': 1, '5': 9, '10': 'toGroupId'},
    const {'1': 'toRoomId', '3': 7, '4': 1, '5': 9, '10': 'toRoomId'},
    const {'1': 'type', '3': 8, '4': 1, '5': 14, '6': '.proto.Message.Type', '10': 'type'},
    const {'1': 'ping', '3': 9, '4': 1, '5': 11, '6': '.proto.Ping', '10': 'ping'},
    const {'1': 'pong', '3': 10, '4': 1, '5': 11, '6': '.proto.Pong', '10': 'pong'},
    const {'1': 'ctime', '3': 11, '4': 1, '5': 3, '10': 'ctime'},
    const {'1': 'req', '3': 1001, '4': 1, '5': 11, '6': '.proto.Request', '10': 'req'},
    const {'1': 'resp', '3': 1002, '4': 1, '5': 11, '6': '.proto.Response', '10': 'resp'},
    const {'1': 'contentMessage', '3': 1003, '4': 1, '5': 11, '6': '.proto.ContentMessage', '10': 'contentMessage'},
    const {'1': 'messageAck', '3': 1004, '4': 1, '5': 11, '6': '.proto.MessageAck', '10': 'messageAck'},
    const {'1': 'notification', '3': 1005, '4': 1, '5': 11, '6': '.proto.Notification', '10': 'notification'},
  ],
  '4': const [Message_Type$json],
};

const Message_Type$json = const {
  '1': 'Type',
  '2': const [
    const {'1': 'Undefined', '2': 0},
    const {'1': 'Ping', '2': 1},
    const {'1': 'Pong', '2': 2},
    const {'1': 'Request', '2': 1001},
    const {'1': 'Response', '2': 1002},
    const {'1': 'ContentMessage', '2': 1003},
    const {'1': 'GroupContentMessage', '2': 1004},
    const {'1': 'RoomContentMessage', '2': 1005},
    const {'1': 'MessageAck', '2': 1006},
    const {'1': 'Notification', '2': 1007},
  ],
};

