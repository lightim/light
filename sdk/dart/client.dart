import 'dart:io';
import './message.pb.dart';

enum IMStatus {
  Unconnected,
  Connected,
  Authed,
  AuthFailed,
}

enum IMErrorCode {
  NetworkException,
  InvalidMessage,
  AuthFailed,
}

class LightClient {
  WebSocket socket;

  /* 连接服务器且认证成功 */
  Function onopen;
  /* 出错 */
  Function onerror;
  /* 链接关闭，出错的情况下会先调用onerror再调用onclose */
  Function onclose;
  /* 收到消息 */
  Function onmessage;

  IMStatus status;

  String appId;
  String token;

  LightClient(this.socket, this.onopen, this.onmessage, this.onclose,
      this.onerror, this.appId, this.token) {
    this.status = IMStatus.Connected;
    this.socket.listen(this.onData,
        onError: this.onError, onDone: this.onDone, cancelOnError: true);

    this.doAuth();
  }

  /* 发送认证消息 */
  doAuth() {
    var m = Message.create();
    m.appId = this.appId;
    m.type = Message_Type.Request;
    var req = Request.create();
    m.req = req;
    req.type = RequestType.Auth;
    var auth = AuthRequest.create();
    req.auth = auth;
    auth.client = '';
    auth.token = this.token;
    this.socket.add(m.writeToBuffer());
  }

  onData(data) {
    var m = Message.fromBuffer(data);
    if (m == null) {
      return;
    }
    print(m);
    if (this.status == IMStatus.Connected) {
      if (m.type != Message_Type.Response ||
          m.resp == null ||
          m.resp.type != RequestType.Auth ||
          m.resp.auth == null) {
        this.onerror(IMErrorCode.InvalidMessage);
        this.onDone();
        return;
      } else if (m.resp.auth.status != AuthResponse_Status.OK) {
        this.onerror(IMErrorCode.AuthFailed);
        this.onDone();
        this.status = IMStatus.AuthFailed;
        return;
      }
      this.status = IMStatus.Authed;
      this.onopen();
      return;
    }
    this.onmessage(m);
  }

  onError(err) {
    print('onError:$err');
    this.onerror(IMErrorCode.NetworkException);
  }

  onDone() {
    this.socket.close();
    this.onclose();
  }

  send(Message m) {
    if (this.status != IMStatus.Authed) {
      return;
    }
    this.socket.add(m.writeToBuffer());
  }

  static Future<LightClient> connect(String url, Function _onopen, _onmessage,
      _onclose, _onerror, String appId, token) async {
    var socket = await WebSocket.connect(url);
    return LightClient(
        socket, _onopen, _onmessage, _onclose, _onerror, appId, token);
  }

  close() {
    this.onDone();
  }
}
