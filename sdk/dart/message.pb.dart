///
//  Generated code. Do not modify.
//  source: message.proto
///
// ignore_for_file: non_constant_identifier_names,library_prefixes,unused_import

// ignore: UNUSED_SHOWN_NAME
import 'dart:core' show int, bool, double, String, List, Map, override;

import 'package:fixnum/fixnum.dart';
import 'package:protobuf/protobuf.dart' as $pb;

import 'message.pbenum.dart';

export 'message.pbenum.dart';

class Ping extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('Ping', package: const $pb.PackageName('proto'))
    ..aInt64(1, 'seq')
    ..hasRequiredFields = false
  ;

  Ping() : super();
  Ping.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  Ping.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  Ping clone() => new Ping()..mergeFromMessage(this);
  Ping copyWith(void Function(Ping) updates) => super.copyWith((message) => updates(message as Ping));
  $pb.BuilderInfo get info_ => _i;
  static Ping create() => new Ping();
  static $pb.PbList<Ping> createRepeated() => new $pb.PbList<Ping>();
  static Ping getDefault() => _defaultInstance ??= create()..freeze();
  static Ping _defaultInstance;
  static void $checkItem(Ping v) {
    if (v is! Ping) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  Int64 get seq => $_getI64(0);
  set seq(Int64 v) { $_setInt64(0, v); }
  bool hasSeq() => $_has(0);
  void clearSeq() => clearField(1);
}

class Pong extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('Pong', package: const $pb.PackageName('proto'))
    ..aInt64(2, 'seq')
    ..hasRequiredFields = false
  ;

  Pong() : super();
  Pong.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  Pong.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  Pong clone() => new Pong()..mergeFromMessage(this);
  Pong copyWith(void Function(Pong) updates) => super.copyWith((message) => updates(message as Pong));
  $pb.BuilderInfo get info_ => _i;
  static Pong create() => new Pong();
  static $pb.PbList<Pong> createRepeated() => new $pb.PbList<Pong>();
  static Pong getDefault() => _defaultInstance ??= create()..freeze();
  static Pong _defaultInstance;
  static void $checkItem(Pong v) {
    if (v is! Pong) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  Int64 get seq => $_getI64(0);
  set seq(Int64 v) { $_setInt64(0, v); }
  bool hasSeq() => $_has(0);
  void clearSeq() => clearField(2);
}

class AuthRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('AuthRequest', package: const $pb.PackageName('proto'))
    ..aOS(2, 'token')
    ..aOS(3, 'client')
    ..hasRequiredFields = false
  ;

  AuthRequest() : super();
  AuthRequest.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  AuthRequest.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  AuthRequest clone() => new AuthRequest()..mergeFromMessage(this);
  AuthRequest copyWith(void Function(AuthRequest) updates) => super.copyWith((message) => updates(message as AuthRequest));
  $pb.BuilderInfo get info_ => _i;
  static AuthRequest create() => new AuthRequest();
  static $pb.PbList<AuthRequest> createRepeated() => new $pb.PbList<AuthRequest>();
  static AuthRequest getDefault() => _defaultInstance ??= create()..freeze();
  static AuthRequest _defaultInstance;
  static void $checkItem(AuthRequest v) {
    if (v is! AuthRequest) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  String get token => $_getS(0, '');
  set token(String v) { $_setString(0, v); }
  bool hasToken() => $_has(0);
  void clearToken() => clearField(2);

  String get client => $_getS(1, '');
  set client(String v) { $_setString(1, v); }
  bool hasClient() => $_has(1);
  void clearClient() => clearField(3);
}

class Request extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('Request', package: const $pb.PackageName('proto'))
    ..e<RequestType>(1, 'type', $pb.PbFieldType.OE, RequestType.Auth, RequestType.valueOf, RequestType.values)
    ..a<AuthRequest>(2, 'auth', $pb.PbFieldType.OM, AuthRequest.getDefault, AuthRequest.create)
    ..hasRequiredFields = false
  ;

  Request() : super();
  Request.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  Request.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  Request clone() => new Request()..mergeFromMessage(this);
  Request copyWith(void Function(Request) updates) => super.copyWith((message) => updates(message as Request));
  $pb.BuilderInfo get info_ => _i;
  static Request create() => new Request();
  static $pb.PbList<Request> createRepeated() => new $pb.PbList<Request>();
  static Request getDefault() => _defaultInstance ??= create()..freeze();
  static Request _defaultInstance;
  static void $checkItem(Request v) {
    if (v is! Request) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  RequestType get type => $_getN(0);
  set type(RequestType v) { setField(1, v); }
  bool hasType() => $_has(0);
  void clearType() => clearField(1);

  AuthRequest get auth => $_getN(1);
  set auth(AuthRequest v) { setField(2, v); }
  bool hasAuth() => $_has(1);
  void clearAuth() => clearField(2);
}

class AuthResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('AuthResponse', package: const $pb.PackageName('proto'))
    ..e<AuthResponse_Status>(1, 'status', $pb.PbFieldType.OE, AuthResponse_Status.Undefined, AuthResponse_Status.valueOf, AuthResponse_Status.values)
    ..aOS(2, 'userId')
    ..hasRequiredFields = false
  ;

  AuthResponse() : super();
  AuthResponse.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  AuthResponse.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  AuthResponse clone() => new AuthResponse()..mergeFromMessage(this);
  AuthResponse copyWith(void Function(AuthResponse) updates) => super.copyWith((message) => updates(message as AuthResponse));
  $pb.BuilderInfo get info_ => _i;
  static AuthResponse create() => new AuthResponse();
  static $pb.PbList<AuthResponse> createRepeated() => new $pb.PbList<AuthResponse>();
  static AuthResponse getDefault() => _defaultInstance ??= create()..freeze();
  static AuthResponse _defaultInstance;
  static void $checkItem(AuthResponse v) {
    if (v is! AuthResponse) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  AuthResponse_Status get status => $_getN(0);
  set status(AuthResponse_Status v) { setField(1, v); }
  bool hasStatus() => $_has(0);
  void clearStatus() => clearField(1);

  String get userId => $_getS(1, '');
  set userId(String v) { $_setString(1, v); }
  bool hasUserId() => $_has(1);
  void clearUserId() => clearField(2);
}

class Response extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('Response', package: const $pb.PackageName('proto'))
    ..e<RequestType>(1, 'type', $pb.PbFieldType.OE, RequestType.Auth, RequestType.valueOf, RequestType.values)
    ..a<AuthResponse>(2, 'auth', $pb.PbFieldType.OM, AuthResponse.getDefault, AuthResponse.create)
    ..hasRequiredFields = false
  ;

  Response() : super();
  Response.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  Response.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  Response clone() => new Response()..mergeFromMessage(this);
  Response copyWith(void Function(Response) updates) => super.copyWith((message) => updates(message as Response));
  $pb.BuilderInfo get info_ => _i;
  static Response create() => new Response();
  static $pb.PbList<Response> createRepeated() => new $pb.PbList<Response>();
  static Response getDefault() => _defaultInstance ??= create()..freeze();
  static Response _defaultInstance;
  static void $checkItem(Response v) {
    if (v is! Response) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  RequestType get type => $_getN(0);
  set type(RequestType v) { setField(1, v); }
  bool hasType() => $_has(0);
  void clearType() => clearField(1);

  AuthResponse get auth => $_getN(1);
  set auth(AuthResponse v) { setField(2, v); }
  bool hasAuth() => $_has(1);
  void clearAuth() => clearField(2);
}

class ContentMessage extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('ContentMessage', package: const $pb.PackageName('proto'))
    ..e<ContentMessage_Type>(1, 'type', $pb.PbFieldType.OE, ContentMessage_Type.Undefined, ContentMessage_Type.valueOf, ContentMessage_Type.values)
    ..m<String, String>(4, 'content', $pb.PbFieldType.OS, $pb.PbFieldType.OS)
    ..hasRequiredFields = false
  ;

  ContentMessage() : super();
  ContentMessage.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  ContentMessage.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  ContentMessage clone() => new ContentMessage()..mergeFromMessage(this);
  ContentMessage copyWith(void Function(ContentMessage) updates) => super.copyWith((message) => updates(message as ContentMessage));
  $pb.BuilderInfo get info_ => _i;
  static ContentMessage create() => new ContentMessage();
  static $pb.PbList<ContentMessage> createRepeated() => new $pb.PbList<ContentMessage>();
  static ContentMessage getDefault() => _defaultInstance ??= create()..freeze();
  static ContentMessage _defaultInstance;
  static void $checkItem(ContentMessage v) {
    if (v is! ContentMessage) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  ContentMessage_Type get type => $_getN(0);
  set type(ContentMessage_Type v) { setField(1, v); }
  bool hasType() => $_has(0);
  void clearType() => clearField(1);

  Map<String, String> get content => $_getMap(1);
}

class MessageAck extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('MessageAck', package: const $pb.PackageName('proto'))
    ..e<MessageAck_Status>(1, 'status', $pb.PbFieldType.OE, MessageAck_Status.Undefined, MessageAck_Status.valueOf, MessageAck_Status.values)
    ..aOS(2, 'msgId')
    ..hasRequiredFields = false
  ;

  MessageAck() : super();
  MessageAck.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  MessageAck.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  MessageAck clone() => new MessageAck()..mergeFromMessage(this);
  MessageAck copyWith(void Function(MessageAck) updates) => super.copyWith((message) => updates(message as MessageAck));
  $pb.BuilderInfo get info_ => _i;
  static MessageAck create() => new MessageAck();
  static $pb.PbList<MessageAck> createRepeated() => new $pb.PbList<MessageAck>();
  static MessageAck getDefault() => _defaultInstance ??= create()..freeze();
  static MessageAck _defaultInstance;
  static void $checkItem(MessageAck v) {
    if (v is! MessageAck) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  MessageAck_Status get status => $_getN(0);
  set status(MessageAck_Status v) { setField(1, v); }
  bool hasStatus() => $_has(0);
  void clearStatus() => clearField(1);

  String get msgId => $_getS(1, '');
  set msgId(String v) { $_setString(1, v); }
  bool hasMsgId() => $_has(1);
  void clearMsgId() => clearField(2);
}

class Notification extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('Notification', package: const $pb.PackageName('proto'))
    ..e<Notification_Type>(1, 'type', $pb.PbFieldType.OE, Notification_Type.Undefined, Notification_Type.valueOf, Notification_Type.values)
    ..m<String, String>(2, 'content', $pb.PbFieldType.OS, $pb.PbFieldType.OS)
    ..aOB(3, 'needAck')
    ..hasRequiredFields = false
  ;

  Notification() : super();
  Notification.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  Notification.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  Notification clone() => new Notification()..mergeFromMessage(this);
  Notification copyWith(void Function(Notification) updates) => super.copyWith((message) => updates(message as Notification));
  $pb.BuilderInfo get info_ => _i;
  static Notification create() => new Notification();
  static $pb.PbList<Notification> createRepeated() => new $pb.PbList<Notification>();
  static Notification getDefault() => _defaultInstance ??= create()..freeze();
  static Notification _defaultInstance;
  static void $checkItem(Notification v) {
    if (v is! Notification) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  Notification_Type get type => $_getN(0);
  set type(Notification_Type v) { setField(1, v); }
  bool hasType() => $_has(0);
  void clearType() => clearField(1);

  Map<String, String> get content => $_getMap(1);

  bool get needAck => $_get(2, false);
  set needAck(bool v) { $_setBool(2, v); }
  bool hasNeedAck() => $_has(2);
  void clearNeedAck() => clearField(3);
}

class Message extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('Message', package: const $pb.PackageName('proto'))
    ..e<Version>(1, 'version', $pb.PbFieldType.OE, Version.Undefined, Version.valueOf, Version.values)
    ..aOS(2, 'id')
    ..aOS(3, 'appId')
    ..aOS(4, 'fromUserId')
    ..aOS(5, 'toUserId')
    ..aOS(6, 'toGroupId')
    ..aOS(7, 'toRoomId')
    ..e<Message_Type>(8, 'type', $pb.PbFieldType.OE, Message_Type.Undefined, Message_Type.valueOf, Message_Type.values)
    ..a<Ping>(9, 'ping', $pb.PbFieldType.OM, Ping.getDefault, Ping.create)
    ..a<Pong>(10, 'pong', $pb.PbFieldType.OM, Pong.getDefault, Pong.create)
    ..aInt64(11, 'ctime')
    ..a<Request>(1001, 'req', $pb.PbFieldType.OM, Request.getDefault, Request.create)
    ..a<Response>(1002, 'resp', $pb.PbFieldType.OM, Response.getDefault, Response.create)
    ..a<ContentMessage>(1003, 'contentMessage', $pb.PbFieldType.OM, ContentMessage.getDefault, ContentMessage.create)
    ..a<MessageAck>(1004, 'messageAck', $pb.PbFieldType.OM, MessageAck.getDefault, MessageAck.create)
    ..a<Notification>(1005, 'notification', $pb.PbFieldType.OM, Notification.getDefault, Notification.create)
    ..hasRequiredFields = false
  ;

  Message() : super();
  Message.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  Message.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  Message clone() => new Message()..mergeFromMessage(this);
  Message copyWith(void Function(Message) updates) => super.copyWith((message) => updates(message as Message));
  $pb.BuilderInfo get info_ => _i;
  static Message create() => new Message();
  static $pb.PbList<Message> createRepeated() => new $pb.PbList<Message>();
  static Message getDefault() => _defaultInstance ??= create()..freeze();
  static Message _defaultInstance;
  static void $checkItem(Message v) {
    if (v is! Message) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  Version get version => $_getN(0);
  set version(Version v) { setField(1, v); }
  bool hasVersion() => $_has(0);
  void clearVersion() => clearField(1);

  String get id => $_getS(1, '');
  set id(String v) { $_setString(1, v); }
  bool hasId() => $_has(1);
  void clearId() => clearField(2);

  String get appId => $_getS(2, '');
  set appId(String v) { $_setString(2, v); }
  bool hasAppId() => $_has(2);
  void clearAppId() => clearField(3);

  String get fromUserId => $_getS(3, '');
  set fromUserId(String v) { $_setString(3, v); }
  bool hasFromUserId() => $_has(3);
  void clearFromUserId() => clearField(4);

  String get toUserId => $_getS(4, '');
  set toUserId(String v) { $_setString(4, v); }
  bool hasToUserId() => $_has(4);
  void clearToUserId() => clearField(5);

  String get toGroupId => $_getS(5, '');
  set toGroupId(String v) { $_setString(5, v); }
  bool hasToGroupId() => $_has(5);
  void clearToGroupId() => clearField(6);

  String get toRoomId => $_getS(6, '');
  set toRoomId(String v) { $_setString(6, v); }
  bool hasToRoomId() => $_has(6);
  void clearToRoomId() => clearField(7);

  Message_Type get type => $_getN(7);
  set type(Message_Type v) { setField(8, v); }
  bool hasType() => $_has(7);
  void clearType() => clearField(8);

  Ping get ping => $_getN(8);
  set ping(Ping v) { setField(9, v); }
  bool hasPing() => $_has(8);
  void clearPing() => clearField(9);

  Pong get pong => $_getN(9);
  set pong(Pong v) { setField(10, v); }
  bool hasPong() => $_has(9);
  void clearPong() => clearField(10);

  Int64 get ctime => $_getI64(10);
  set ctime(Int64 v) { $_setInt64(10, v); }
  bool hasCtime() => $_has(10);
  void clearCtime() => clearField(11);

  Request get req => $_getN(11);
  set req(Request v) { setField(1001, v); }
  bool hasReq() => $_has(11);
  void clearReq() => clearField(1001);

  Response get resp => $_getN(12);
  set resp(Response v) { setField(1002, v); }
  bool hasResp() => $_has(12);
  void clearResp() => clearField(1002);

  ContentMessage get contentMessage => $_getN(13);
  set contentMessage(ContentMessage v) { setField(1003, v); }
  bool hasContentMessage() => $_has(13);
  void clearContentMessage() => clearField(1003);

  MessageAck get messageAck => $_getN(14);
  set messageAck(MessageAck v) { setField(1004, v); }
  bool hasMessageAck() => $_has(14);
  void clearMessageAck() => clearField(1004);

  Notification get notification => $_getN(15);
  set notification(Notification v) { setField(1005, v); }
  bool hasNotification() => $_has(15);
  void clearNotification() => clearField(1005);
}

