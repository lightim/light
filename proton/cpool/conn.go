package cpool

import (
	"errors"
	"time"

	"gitlab.com/lightim/light/log"
	"gitlab.com/lightim/light/proto"
	"gitlab.com/lightim/light/queue"
	"gitlab.com/lightim/light/session"
)

var (
	ConnTimeout = time.Minute * 5
)

var (
	ErrMessageInvalid = errors.New("invalid message")
)

/* 消息处理循环 */
func (p *CPool) connloop(appId, userId string, c proto.Conn) {
	defer p.DelAndClose(appId, userId)
	defer queue.PubEvent(appId, userId, p.rpcAddr, queue.EventTypeDisconnect)

	queue.PubEvent(appId, userId, p.rpcAddr, queue.EventTypeConnect)

	for {
		if err := session.SetSession(appId, userId, p.rpcAddr, ConnTimeout); err != nil {
			log.Errorf("SetSession Error:%v", err)
			break
		}
		c.SetReadDeadline(time.Now().Add(ConnTimeout))
		if m, err := c.Read(); err != nil {
			break
		} else if m.Id == "" || m.Version != proto.Version_V1 {
			log.Warnf("Invalid Message: %v", m)
		} else {
			var err error
			m.AppId = appId
			m.FromUserId = userId
			m.Ctime = time.Now().UnixNano()
			switch m.Type {
			case proto.Message_ContentMessage:
				err = p.handleContentMessage(c, m)
			case proto.Message_GroupContentMessage:
				err = p.handleGroupContentMessage(c, m)
			case proto.Message_RoomContentMessage:
				err = p.handleRoomContentMessage(c, m)
			case proto.Message_MessageAck:
				err = p.handleMessageAck(c, m)
			}
			if err != nil { /* 消息处理出错，很可能是客户端接入的协议有问题，断开链接 */
				log.Warnf("Message Error: %v", m)
				break
			}
		}
	}
}

func (p *CPool) handleContentMessage(c proto.Conn, m *proto.Message) error {
	if m.ToUserId == "" || m.Content == "" {
		return ErrMessageInvalid
	}

	queue.PubMessage(m)
	return nil
}

func (p *CPool) handleGroupContentMessage(c proto.Conn, m *proto.Message) error {
	if m.Content == "" || m.ToUserId == "" {
		return ErrMessageInvalid
	}

	queue.PubMessage(m)
	return nil
}

func (p *CPool) handleRoomContentMessage(c proto.Conn, m *proto.Message) error {
	if m.Content == "" || m.ToUserId == "" {
		return ErrMessageInvalid
	}

	queue.PubMessage(m)
	return nil
}

func (p *CPool) handleMessageAck(c proto.Conn, m *proto.Message) error {
	if m.Content == "" {
		return ErrMessageInvalid
	}

	queue.PubMessage(m)
	return nil
}
