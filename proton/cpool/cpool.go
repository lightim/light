package cpool

import (
	"sync"
	"time"

	"encoding/json"

	userdb "gitlab.com/lightim/light/database/user"
	"gitlab.com/lightim/light/log"
	"gitlab.com/lightim/light/proto"
	"gitlab.com/lightim/light/proton/service"
	"gitlab.com/lightim/light/session"
	"gitlab.com/lightim/light/x"
)

/*
 * 客户端链接池
 */
type CPool struct {
	sync.Mutex
	ver     proto.Version
	clients map[string]map[string]proto.Conn
	rpcAddr string
}

func New(rpcAddr string) *CPool {
	return &CPool{
		ver:     proto.Version_V1,
		clients: make(map[string]map[string]proto.Conn),
		rpcAddr: rpcAddr,
	}
}

/* 删除某个连接 */
func (p *CPool) Del(appId, userId string) proto.Conn {
	p.Lock()
	defer p.Unlock()

	app := p.clients[appId]
	if app == nil {
		return nil
	}
	c := app[userId]
	if c == nil {
		return nil
	}
	log.Infof("%s.%s disconnected", appId, userId)

	session.DelSession(appId, userId)
	delete(app, userId)
	return c
}

/* 删除并关闭连接 */
func (p *CPool) DelAndClose(appid, userid string) {
	c := p.Del(appid, userid)
	if c != nil {
		c.Close()
	}
}

func (p *CPool) Get(appid, userid string) proto.Conn {
	p.Lock()
	defer p.Unlock()

	app := p.clients[appid]
	if app == nil {
		return nil
	}
	c := app[userid]
	if c == nil {
		return nil
	}
	return c
}

/* 添加一个连接 */
func (p *CPool) Add(appId, userId string, c proto.Conn) {
	log.Infof("%s.%s connected", appId, userId)

	if saddr := session.GetSession(appId, userId); saddr != "" {
		if err := service.KickOff(saddr, appId, userId); err != nil {
			log.Errorf("kick off %s.%s error:%v", appId, userId, err)
		} else {
			log.Infof("%s.%s kicked off successfully", appId, userId)
		}
	}

	defer p.Unlock()
	p.Lock()

	app := p.clients[appId]
	if app == nil {
		app = make(map[string]proto.Conn)
		p.clients[appId] = app
	}
	app[userId] = c

	go p.connloop(appId, userId, c)
}

func (p *CPool) Close() {
	defer p.Unlock()
	p.Lock()
	for appID, users := range p.clients {
		for userID := range users {
			session.DelSession(appID, userID)
		}
	}
}

type AuthRequest struct {
	Client string `json:"client"`
	Token  string `json:"token"`
}

/* 认证状态 */
const (
	AuthStatusOK            = 0
	AuthStatusAppNotFound   = 1
	AuthStatusTokenNotFound = 2
)

type AuthResponse struct {
	Status int    `json:"status"`
	UserID string `json:"user_id"`
}

/* 一个新连接的第一个请求必须是用户认证 */
func (p *CPool) Wait4Auth(c proto.Conn) {
	c.SetReadDeadline(time.Now().Add(time.Second * 30))
	m, err := c.Read()
	if err != nil || m == nil {
		log.Warnf("failed to read auth message: %v", err)
		c.Close()
		return
	}
	if m.Type != proto.Message_AuthRequest || m.Content == "" {
		log.Warnf("invalid auth message")
		c.Close()
		return
	}
	appid := m.AppId

	var req AuthRequest
	var resp AuthResponse
	if err := json.Unmarshal([]byte(m.Content), &req); err != nil {
		log.Warnf("invalid auth message")
		c.Close()
		return
	}
	token := req.Token
	client := req.Client
	userID := ""

	pm := &proto.Message{
		Version: p.ver,
		Id:      x.UUID(),
		Type:    proto.Message_AuthResponse,
		Ctime:   time.Now().UnixNano(),
	}

	app, err := userdb.GetAppById(appid)
	if err != nil {
		c.Close()
		return
	} else if app == nil {
		resp.Status = AuthStatusAppNotFound
		content, _ := json.Marshal(resp)
		pm.Content = string(content)
		c.Write(pm)
		c.Close()
		return
	}

	if app.AllowAnonymous && token == "" {
		userID = "a-" + x.UUID()
	} else {
		sess, err := userdb.GetTokenWithClient(token, client)
		if err != nil {
			c.Close()
			return
		} else if sess == nil || sess.AppId != app.AppId {
			resp.Status = AuthStatusTokenNotFound
			content, _ := json.Marshal(resp)
			pm.Content = string(content)
			c.Write(pm)
			c.Close()
			return
		} else {
			userID = sess.UserId
		}
	}
	resp.Status = AuthStatusOK
	resp.UserID = userID
	content, _ := json.Marshal(resp)
	pm.Content = string(content)
	if err := c.Write(pm); err != nil {
		c.Close()
		return
	}
	c.SetReadDeadline(time.Time{})
	p.Add(app.AppId, userID, c)
}
