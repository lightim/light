package cpool

import (
	"context"
	"time"

	"encoding/json"
	gproto "github.com/golang/protobuf/proto"
	"gitlab.com/lightim/light/proto"
	"gitlab.com/lightim/light/proton/service"
	"gitlab.com/lightim/light/x"
)

func (p *CPool) KickOff(ctx context.Context, in *service.KickOffRequest) (*service.VOID, error) {
	appId := in.AppId
	userid := in.UserId

	c := p.Del(appId, userid)

	content, _ := json.Marshal(map[string]interface{}{
		"type": 1,
	})

	if c != nil {
		c.Write(&proto.Message{
			Id:      x.UUID(),
			Type:    proto.Message_Notification,
			Version: p.ver,
			Ctime:   time.Now().UnixNano(),
			Content: string(content),
		})
		c.Close()
	}
	return &service.VOID{}, nil
}

func (p *CPool) SendMessage(ctx context.Context, in *service.SendMessageRequest) (*service.SendMessageReply, error) {
	appid := in.AppId
	userid := in.UserId

	c := p.Get(appid, userid)

	if c == nil {
		return &service.SendMessageReply{
			Status: service.SendMessageReply_NotFound,
		}, nil
	}

	var m proto.Message
	if err := gproto.Unmarshal(in.Message, &m); err != nil {
		return &service.SendMessageReply{
			Status: service.SendMessageReply_Error,
		}, nil
	}

	if err := c.Write(&m); err != nil {
		defer p.DelAndClose(appid, userid)
		return &service.SendMessageReply{
			Status: service.SendMessageReply_Error,
		}, nil
	}

	return &service.SendMessageReply{
		Status: service.SendMessageReply_OK,
	}, nil
}
