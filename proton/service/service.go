package service

import (
	"context"

	proto "github.com/golang/protobuf/proto"
	"gitlab.com/lightim/light/log"
	"google.golang.org/grpc"
)

var clients map[string]ProtonClient

func init() {
	clients = make(map[string]ProtonClient)
}

func GetClient(addr string) (ProtonClient, error) {
	c := clients[addr]
	if c != nil {
		return c, nil
	}
	cc, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		log.Errorf("failed to connect to grpc:%s", addr)
		return nil, err
	}
	c = NewProtonClient(cc)
	clients[addr] = c
	return c, nil
}

func KickOff(addr, appid, userid string) error {
	c, err := GetClient(addr)
	if err != nil {
		return err
	}
	_, err = c.KickOff(context.Background(), &KickOffRequest{
		AppId:  appid,
		UserId: userid,
	})
	if err != nil {
		log.Errorf("faield to kickoff %s", userid)
	}
	return err
}

func SendMessage(addr, appid, userid string, m proto.Message) (SendMessageReply_Status, error) {
	c, err := GetClient(addr)
	if err != nil {
		return 0, err
	}

	message, err := proto.Marshal(m)
	if err != nil {
		return 0, err
	}

	r, err := c.SendMessage(context.Background(), &SendMessageRequest{
		AppId:   appid,
		UserId:  userid,
		Message: message,
	})
	if err != nil {
		return 0, err
	}
	return r.Status, nil
}
