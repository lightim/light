// Code generated by protoc-gen-go. DO NOT EDIT.
// source: service/proton.proto

package service

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type SendMessageReply_Status int32

const (
	SendMessageReply_OK       SendMessageReply_Status = 0
	SendMessageReply_NotFound SendMessageReply_Status = 1
	SendMessageReply_Error    SendMessageReply_Status = 2
)

var SendMessageReply_Status_name = map[int32]string{
	0: "OK",
	1: "NotFound",
	2: "Error",
}

var SendMessageReply_Status_value = map[string]int32{
	"OK":       0,
	"NotFound": 1,
	"Error":    2,
}

func (x SendMessageReply_Status) String() string {
	return proto.EnumName(SendMessageReply_Status_name, int32(x))
}

func (SendMessageReply_Status) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_b55e3e9226b0e2a6, []int{3, 0}
}

type VOID struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *VOID) Reset()         { *m = VOID{} }
func (m *VOID) String() string { return proto.CompactTextString(m) }
func (*VOID) ProtoMessage()    {}
func (*VOID) Descriptor() ([]byte, []int) {
	return fileDescriptor_b55e3e9226b0e2a6, []int{0}
}

func (m *VOID) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_VOID.Unmarshal(m, b)
}
func (m *VOID) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_VOID.Marshal(b, m, deterministic)
}
func (m *VOID) XXX_Merge(src proto.Message) {
	xxx_messageInfo_VOID.Merge(m, src)
}
func (m *VOID) XXX_Size() int {
	return xxx_messageInfo_VOID.Size(m)
}
func (m *VOID) XXX_DiscardUnknown() {
	xxx_messageInfo_VOID.DiscardUnknown(m)
}

var xxx_messageInfo_VOID proto.InternalMessageInfo

type KickOffRequest struct {
	AppId                string   `protobuf:"bytes,1,opt,name=appId,proto3" json:"appId,omitempty"`
	UserId               string   `protobuf:"bytes,2,opt,name=userId,proto3" json:"userId,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *KickOffRequest) Reset()         { *m = KickOffRequest{} }
func (m *KickOffRequest) String() string { return proto.CompactTextString(m) }
func (*KickOffRequest) ProtoMessage()    {}
func (*KickOffRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_b55e3e9226b0e2a6, []int{1}
}

func (m *KickOffRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_KickOffRequest.Unmarshal(m, b)
}
func (m *KickOffRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_KickOffRequest.Marshal(b, m, deterministic)
}
func (m *KickOffRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_KickOffRequest.Merge(m, src)
}
func (m *KickOffRequest) XXX_Size() int {
	return xxx_messageInfo_KickOffRequest.Size(m)
}
func (m *KickOffRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_KickOffRequest.DiscardUnknown(m)
}

var xxx_messageInfo_KickOffRequest proto.InternalMessageInfo

func (m *KickOffRequest) GetAppId() string {
	if m != nil {
		return m.AppId
	}
	return ""
}

func (m *KickOffRequest) GetUserId() string {
	if m != nil {
		return m.UserId
	}
	return ""
}

type SendMessageRequest struct {
	AppId                string   `protobuf:"bytes,1,opt,name=appId,proto3" json:"appId,omitempty"`
	UserId               string   `protobuf:"bytes,2,opt,name=userId,proto3" json:"userId,omitempty"`
	Message              []byte   `protobuf:"bytes,3,opt,name=message,proto3" json:"message,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *SendMessageRequest) Reset()         { *m = SendMessageRequest{} }
func (m *SendMessageRequest) String() string { return proto.CompactTextString(m) }
func (*SendMessageRequest) ProtoMessage()    {}
func (*SendMessageRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_b55e3e9226b0e2a6, []int{2}
}

func (m *SendMessageRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SendMessageRequest.Unmarshal(m, b)
}
func (m *SendMessageRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SendMessageRequest.Marshal(b, m, deterministic)
}
func (m *SendMessageRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SendMessageRequest.Merge(m, src)
}
func (m *SendMessageRequest) XXX_Size() int {
	return xxx_messageInfo_SendMessageRequest.Size(m)
}
func (m *SendMessageRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_SendMessageRequest.DiscardUnknown(m)
}

var xxx_messageInfo_SendMessageRequest proto.InternalMessageInfo

func (m *SendMessageRequest) GetAppId() string {
	if m != nil {
		return m.AppId
	}
	return ""
}

func (m *SendMessageRequest) GetUserId() string {
	if m != nil {
		return m.UserId
	}
	return ""
}

func (m *SendMessageRequest) GetMessage() []byte {
	if m != nil {
		return m.Message
	}
	return nil
}

type SendMessageReply struct {
	Status               SendMessageReply_Status `protobuf:"varint,1,opt,name=status,proto3,enum=service.SendMessageReply_Status" json:"status,omitempty"`
	XXX_NoUnkeyedLiteral struct{}                `json:"-"`
	XXX_unrecognized     []byte                  `json:"-"`
	XXX_sizecache        int32                   `json:"-"`
}

func (m *SendMessageReply) Reset()         { *m = SendMessageReply{} }
func (m *SendMessageReply) String() string { return proto.CompactTextString(m) }
func (*SendMessageReply) ProtoMessage()    {}
func (*SendMessageReply) Descriptor() ([]byte, []int) {
	return fileDescriptor_b55e3e9226b0e2a6, []int{3}
}

func (m *SendMessageReply) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SendMessageReply.Unmarshal(m, b)
}
func (m *SendMessageReply) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SendMessageReply.Marshal(b, m, deterministic)
}
func (m *SendMessageReply) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SendMessageReply.Merge(m, src)
}
func (m *SendMessageReply) XXX_Size() int {
	return xxx_messageInfo_SendMessageReply.Size(m)
}
func (m *SendMessageReply) XXX_DiscardUnknown() {
	xxx_messageInfo_SendMessageReply.DiscardUnknown(m)
}

var xxx_messageInfo_SendMessageReply proto.InternalMessageInfo

func (m *SendMessageReply) GetStatus() SendMessageReply_Status {
	if m != nil {
		return m.Status
	}
	return SendMessageReply_OK
}

func init() {
	proto.RegisterEnum("service.SendMessageReply_Status", SendMessageReply_Status_name, SendMessageReply_Status_value)
	proto.RegisterType((*VOID)(nil), "service.VOID")
	proto.RegisterType((*KickOffRequest)(nil), "service.KickOffRequest")
	proto.RegisterType((*SendMessageRequest)(nil), "service.SendMessageRequest")
	proto.RegisterType((*SendMessageReply)(nil), "service.SendMessageReply")
}

func init() { proto.RegisterFile("service/proton.proto", fileDescriptor_b55e3e9226b0e2a6) }

var fileDescriptor_b55e3e9226b0e2a6 = []byte{
	// 268 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x12, 0x29, 0x4e, 0x2d, 0x2a,
	0xcb, 0x4c, 0x4e, 0xd5, 0x2f, 0x28, 0xca, 0x2f, 0xc9, 0xcf, 0xd3, 0x03, 0x53, 0x42, 0xec, 0x50,
	0x51, 0x25, 0x36, 0x2e, 0x96, 0x30, 0x7f, 0x4f, 0x17, 0x25, 0x3b, 0x2e, 0x3e, 0xef, 0xcc, 0xe4,
	0x6c, 0xff, 0xb4, 0xb4, 0xa0, 0xd4, 0xc2, 0xd2, 0xd4, 0xe2, 0x12, 0x21, 0x11, 0x2e, 0xd6, 0xc4,
	0x82, 0x02, 0xcf, 0x14, 0x09, 0x46, 0x05, 0x46, 0x0d, 0xce, 0x20, 0x08, 0x47, 0x48, 0x8c, 0x8b,
	0xad, 0xb4, 0x38, 0xb5, 0xc8, 0x33, 0x45, 0x82, 0x09, 0x2c, 0x0c, 0xe5, 0x29, 0xc5, 0x70, 0x09,
	0x05, 0xa7, 0xe6, 0xa5, 0xf8, 0xa6, 0x16, 0x17, 0x27, 0xa6, 0xa7, 0x92, 0x65, 0x86, 0x90, 0x04,
	0x17, 0x7b, 0x2e, 0x44, 0xbf, 0x04, 0xb3, 0x02, 0xa3, 0x06, 0x4f, 0x10, 0x8c, 0xab, 0x54, 0xce,
	0x25, 0x80, 0x62, 0x7a, 0x41, 0x4e, 0xa5, 0x90, 0x05, 0x17, 0x5b, 0x71, 0x49, 0x62, 0x49, 0x69,
	0x31, 0xd8, 0x70, 0x3e, 0x23, 0x05, 0x3d, 0xa8, 0x9f, 0xf4, 0xd0, 0x95, 0xea, 0x05, 0x83, 0xd5,
	0x05, 0x41, 0xd5, 0x2b, 0x69, 0x72, 0xb1, 0x41, 0x44, 0x84, 0xd8, 0xb8, 0x98, 0xfc, 0xbd, 0x05,
	0x18, 0x84, 0x78, 0xb8, 0x38, 0xfc, 0xf2, 0x4b, 0xdc, 0xf2, 0x4b, 0xf3, 0x52, 0x04, 0x18, 0x85,
	0x38, 0xb9, 0x58, 0x5d, 0x8b, 0x8a, 0xf2, 0x8b, 0x04, 0x98, 0x8c, 0xda, 0x18, 0xb9, 0xd8, 0x02,
	0xc0, 0x01, 0x27, 0x64, 0xcc, 0xc5, 0x0e, 0x0d, 0x21, 0x21, 0x71, 0xb8, 0x55, 0xa8, 0x61, 0x26,
	0xc5, 0x0b, 0x97, 0x00, 0x07, 0x2a, 0x83, 0x90, 0x3b, 0x17, 0x37, 0x92, 0x6b, 0x84, 0xa4, 0xb1,
	0xbb, 0x11, 0xa2, 0x59, 0x12, 0xa7, 0x07, 0x94, 0x18, 0x92, 0xd8, 0xc0, 0xf1, 0x66, 0x0c, 0x08,
	0x00, 0x00, 0xff, 0xff, 0x68, 0xd3, 0x0e, 0xec, 0xcf, 0x01, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// ProtonClient is the client API for Proton service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type ProtonClient interface {
	// 在别处登录而踢出
	KickOff(ctx context.Context, in *KickOffRequest, opts ...grpc.CallOption) (*VOID, error)
	// 发送消息
	SendMessage(ctx context.Context, in *SendMessageRequest, opts ...grpc.CallOption) (*SendMessageReply, error)
}

type protonClient struct {
	cc *grpc.ClientConn
}

func NewProtonClient(cc *grpc.ClientConn) ProtonClient {
	return &protonClient{cc}
}

func (c *protonClient) KickOff(ctx context.Context, in *KickOffRequest, opts ...grpc.CallOption) (*VOID, error) {
	out := new(VOID)
	err := c.cc.Invoke(ctx, "/service.Proton/KickOff", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *protonClient) SendMessage(ctx context.Context, in *SendMessageRequest, opts ...grpc.CallOption) (*SendMessageReply, error) {
	out := new(SendMessageReply)
	err := c.cc.Invoke(ctx, "/service.Proton/SendMessage", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ProtonServer is the server API for Proton service.
type ProtonServer interface {
	// 在别处登录而踢出
	KickOff(context.Context, *KickOffRequest) (*VOID, error)
	// 发送消息
	SendMessage(context.Context, *SendMessageRequest) (*SendMessageReply, error)
}

func RegisterProtonServer(s *grpc.Server, srv ProtonServer) {
	s.RegisterService(&_Proton_serviceDesc, srv)
}

func _Proton_KickOff_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(KickOffRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProtonServer).KickOff(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/service.Proton/KickOff",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProtonServer).KickOff(ctx, req.(*KickOffRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Proton_SendMessage_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SendMessageRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProtonServer).SendMessage(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/service.Proton/SendMessage",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProtonServer).SendMessage(ctx, req.(*SendMessageRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _Proton_serviceDesc = grpc.ServiceDesc{
	ServiceName: "service.Proton",
	HandlerType: (*ProtonServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "KickOff",
			Handler:    _Proton_KickOff_Handler,
		},
		{
			MethodName: "SendMessage",
			Handler:    _Proton_SendMessage_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "service/proton.proto",
}
