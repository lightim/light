package main

import (
	"net"
	"net/http"
	"os"
	"os/signal"

	"github.com/gorilla/websocket"
	"gitlab.com/lightim/light/cache"
	"gitlab.com/lightim/light/config"
	"gitlab.com/lightim/light/database"
	"gitlab.com/lightim/light/log"
	"gitlab.com/lightim/light/proto"
	"gitlab.com/lightim/light/proton/cpool"
	"gitlab.com/lightim/light/proton/service"
	"gitlab.com/lightim/light/queue"
	"gitlab.com/lightim/light/x"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	AppName = "proton"
)

func init() {
	initConfig()
	initLog()
	initDatabase()
	initCache()
	initQueue()
}

func initConfig() {
	config.Init(AppName)
}

func initLog() {
	log.Init()
}

func initDatabase() {
	url := config.GetString("db.mongo.url")
	dbname := config.GetString("db.mongo.dbname")
	database.InitMongoDB(url, dbname)
}

func initCache() {
	addr := config.GetString("cache.redis.addr")
	password := config.GetString("cache.redis.password")
	db := config.GetInt("cache.redis.db")
	cache.Init(addr, password, db)
}

func initQueue() {
	url := config.GetString("queue.rabbitmq.url")
	queue.InitConn(url)
	queue.InitEventQueue()
	queue.InitMessageQueue()
}

func main() {
	wsListenAddr := config.GetString("listen")

	/* 自动获取内网地址并监听 */
	localIPs, err := x.FindLocalIPs()
	if err != nil {
		panic(err)
	} else if len(localIPs) == 0 {
		panic("no private ip found!")
	}

	rpcListener, err := net.Listen("tcp", localIPs[0]+":0")
	if err != nil {
		panic(err)
	}

	rpcListenAddr := rpcListener.Addr().String()
	log.Infof("listen rpc on %s", rpcListenAddr)

	cp := cpool.New(rpcListenAddr)
	defer cp.Close()

	/* 启用websocket */
	upgrader := websocket.Upgrader{
		CheckOrigin: func(*http.Request) bool {
			return true
		},
	}
	http.HandleFunc("/light", func(w http.ResponseWriter, r *http.Request) {
		if c, err := upgrader.Upgrade(w, r, nil); err != nil {
			log.Warnf("websocket upgrade error:%v", err)
		} else {
			log.Debugf("new connection from %s", c.RemoteAddr().String())
			go cp.Wait4Auth(proto.NewWSConn(c))
		}
	})
	go func() {
		log.Infof("listen websocket on %s", wsListenAddr)
		if err := http.ListenAndServe(wsListenAddr, nil); err != nil {
			panic(err)
		}
	}()

	/* 注册grpc */
	s := grpc.NewServer()
	service.RegisterProtonServer(s, cp)
	reflection.Register(s)

	go func() {
		if err := s.Serve(rpcListener); err != nil {
			panic(err)
		}
	}()

	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, os.Kill)
	<-c
}
