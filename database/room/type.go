package room

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

/* 临时群在用户断开链接后将会删除 */

type RoomUser struct {
	Id     primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	AppId  string             `bson:"appId" json:"app_id"`
	RoomId string             `bson:"roomId" json:"room_id"`
	UserId string             `bson:"userId" json:"user_id"`
	Temp   bool               `bson:"temp" json:"temp"`
	UTime  time.Time          `bson:"utime" json:"utime"`
	CTime  time.Time          `bson:"ctime" json:"ctime"`
}
