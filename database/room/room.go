package room

import (
	"time"

	"gitlab.com/lightim/light/database"
	"gitlab.com/lightim/light/log"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	CollectionRoomUser = "room_user"
)

func GetRoomUserByRUId(appId, roomId, userId string) (*RoomUser, error) {
	c := database.C(CollectionRoomUser)

	var gu RoomUser

	query := bson.M{
		"appId":  appId,
		"roomId": roomId,
		"userId": userId,
	}

	if err := c.FindOne(nil, query).Decode(&gu); err != nil {
		if err != mongo.ErrNoDocuments {
			log.Errorf("GetRoomUserByRUId Error: %v", err)
			return nil, err
		}
		return nil, nil
	}
	return &gu, nil
}

func FindRoomUsers(appId, groupId string) ([]*RoomUser, error) {
	c := database.C(CollectionRoomUser)

	users := make([]*RoomUser, 0)

	query := bson.M{
		"appId":  appId,
		"roomId": groupId,
	}

	if c, err := c.Find(nil, query); err != nil {
		log.Errorf("FindRoomUsers Error: %v", err)
		return nil, err
	} else {
		for c.Next(nil) {
			var user RoomUser
			if err := c.Decode(&user); err != nil {
				log.Errorf("Decode RoomUser Error:%v", err)
				return nil, err
			}
			users = append(users, &user)
		}
	}

	return users, nil
}

func CreateRoomUsers(appId, roomId string, userIds []string, temp bool) error {
	c := database.C(CollectionRoomUser)

	for _, userId := range userIds {
		user := RoomUser{
			AppId:  appId,
			RoomId: roomId,
			UserId: userId,
			Temp:   temp,
			UTime:  time.Now(),
			CTime:  time.Now(),
		}
		if _, err := c.ReplaceOne(nil, bson.M{"appId": appId, "roomId": roomId, "userId": userId}, &user, options.Replace().SetUpsert(true)); err != nil {
			log.Errorf("CreateRoomUsers Error:%v", err)
		}
	}

	return nil
}

func DeleteTempRoomUsers(appId, userId string) error {
	c := database.C(CollectionRoomUser)

	selector := bson.M{
		"appId":  appId,
		"userId": userId,
		"temp":   true,
	}
	if _, err := c.DeleteMany(nil, selector); err != nil {
		log.Errorf("DeleteTempRoomUsers Error: %v", err)
		return err
	}
	return nil
}

func DeleteRoomUsers(appId, roomId string, userIds []string) error {
	c := database.C(CollectionRoomUser)

	for _, userId := range userIds {
		query := bson.M{
			"appId":  appId,
			"roomId": roomId,
			"userId": userId,
		}
		if _, err := c.DeleteMany(nil, query); err != nil {
			log.Errorf("DeleteRoomUsers Error: %v", err)
			return err
		}
	}
	return nil
}
