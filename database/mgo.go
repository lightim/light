package database

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

var mc *mongo.Client
var mdb *mongo.Database

func InitMongoDB(url, dbname string) error {
	var err error
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	opts := options.Client().ApplyURI(url)
	mc, err = mongo.Connect(ctx, opts)
	if err != nil {
		panic(err)
	}
	if err := mc.Ping(ctx, readpref.Primary()); err != nil {
		panic(err)
	}
	mdb = mc.Database(dbname)
	return nil
}

func C(c string) *mongo.Collection {
	return mdb.Collection(c)
}
