package message

import (
	"encoding/json"
	"gitlab.com/lightim/light/database"
	"gitlab.com/lightim/light/log"
	"gitlab.com/lightim/light/proto"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

const (
	CollectionMessage = "message"
)

func GetMessageById(id string) (*Message, error) {
	c := database.C(CollectionMessage)

	var m Message

	query := bson.M{
		"id": id,
	}
	if err := c.FindOne(nil, query).Decode(&m); err != nil {
		if err != mongo.ErrNoDocuments {
			log.Errorf("GetMessageById Error:%v", err)
			return nil, err
		}
		return nil, nil
	}
	return &m, nil
}

/* 获取针对某一用户，未发送成功的消息 */
func FindMessageUnsent(appId, userId string, st time.Time) ([]*Message, error) {
	c := database.C(CollectionMessage)

	query := bson.M{
		"appId":    appId,
		"toUserId": userId,
		"sent":     false,
		"ctime": bson.M{
			"$gte": st,
		},
	}

	messages := make([]*Message, 0)
	if c, err := c.Find(nil, query, (&options.FindOptions{}).SetSort(bson.M{"ctime": 1})); err != nil {
		log.Errorf("FindMessageUnsent Error:%v", err)
		return nil, err
	} else {
		for c.Next(nil) {
			var m Message
			if err := c.Decode(&m); err != nil {
				log.Errorf("Decode Messages Error:%v", err)
				return nil, err
			}
			messages = append(messages, &m)
		}
	}
	return messages, nil
}

func UpdateMessageSent(mid string, sent bool) error {
	c := database.C(CollectionMessage)

	query := bson.M{"id": mid}
	set := bson.M{
		"$set": bson.M{"sent": sent, "stime": time.Now()},
	}
	if _, err := c.UpdateOne(nil, query, set); err != nil {
		log.Errorf("UpdateMessageSent Error:%v", err)
		return err
	}
	return nil
}

func InsertMessage(m *Message) error {
	if m.Sent {
		m.STime = time.Now()
	}
	c := database.C(CollectionMessage)
	if _, err := c.InsertOne(nil, m); err != nil {
		log.Errorf("InsertMessage Error:%v", err)
		return err
	}
	return nil
}

func InsertNotification(m *proto.Message, sent bool) error {
	content := make(map[string]interface{})
	if err := json.Unmarshal([]byte(m.Content), &content); err != nil {
		return err
	}
	bm := Message{
		Version:    int(m.Version),
		Id:         m.Id,
		Type:       int(m.Type),
		AppId:      m.AppId,
		FromUserId: m.FromUserId,
		ToUserId:   m.ToUserId,
		Sent:       sent,
		CTime:      time.Unix(0, m.Ctime),
		Content:    content,
		NeedAck:    m.NeedAck,
	}
	return InsertMessage(&bm)
}

func InsertContentMessage(m *proto.Message, sent bool) error {
	content := make(map[string]interface{})
	if err := json.Unmarshal([]byte(m.Content), &content); err != nil {
		return err
	}
	bm := Message{
		Version:    int(m.Version),
		Id:         m.Id,
		Type:       int(m.Type),
		AppId:      m.AppId,
		FromUserId: m.FromUserId,
		ToUserId:   m.ToUserId,
		Sent:       sent,
		CTime:      time.Unix(0, m.Ctime),
		Content:    content,
		NeedAck:    m.NeedAck,
	}
	return InsertMessage(&bm)
}

func InsertContentMessageAck(m *proto.Message, sent bool) error {
	content := make(map[string]interface{})
	if err := json.Unmarshal([]byte(m.Content), &content); err != nil {
		return err
	}
	bm := Message{
		Version:    int(m.Version),
		Id:         m.Id,
		Type:       int(m.Type),
		AppId:      m.AppId,
		FromUserId: m.FromUserId,
		ToUserId:   m.ToUserId,
		Sent:       sent,
		CTime:      time.Unix(0, m.Ctime),
		Content:    content,
	}
	return InsertMessage(&bm)
}
