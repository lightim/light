package message

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Message struct {
	BId        primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	Version    int                `bson:"version"`
	Id         string             `bson:"id"`
	AppId      string             `bson:"appId"`
	FromUserId string             `bson:"fromUserId"` /* 发送方的id，如果是服务器下发的消息，则为空 */
	ToUserId   string             `bson:"toUserId"`   /* 目标用户的Id */
	Type       int                `bson:"type"`
	Sent       bool               `bson:"sent"` /* 是否已经发送成功，部分数据需要ack响应后才更新该字段 */

	STime time.Time `bson:"stime"` /* 发送成功的时间 */
	CTime time.Time `bson:"ctime"`

	Content map[string]interface{} `bson:"content""`
	NeedAck bool                   `bson:"need_ack"`
}
