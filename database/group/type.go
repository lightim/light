package group

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type GroupUser struct {
	Id      primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	AppId   string             `bson:"appId" json:"app_id"`
	GroupId string             `bson:"groupId" json:"group_id"`
	UserId  string             `bson:"userId" json:"user_id"`
	UTime   time.Time          `bson:"utime" json:"utime"`
	CTime   time.Time          `bson:"ctime" json:"ctime"`
}
