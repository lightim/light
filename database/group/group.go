package group

import (
	"time"

	"gitlab.com/lightim/light/database"
	"gitlab.com/lightim/light/log"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	CollectionGroupUser = "group_user"
)

func GetGroupUserByGUId(appId, groupId, userId string) (*GroupUser, error) {
	c := database.C(CollectionGroupUser)

	var gu GroupUser

	query := bson.M{
		"appId":   appId,
		"groupId": groupId,
		"userId":  userId,
	}

	if err := c.FindOne(nil, query).Decode(&gu); err != nil {
		if err != mongo.ErrNoDocuments {
			log.Errorf("GetGroupUserByGUId Error: %v", err)
			return nil, err
		}
		return nil, nil
	}
	return &gu, nil
}

func FindGroupUsers(appId, groupId string) ([]*GroupUser, error) {
	c := database.C(CollectionGroupUser)

	users := make([]*GroupUser, 0)

	query := bson.M{
		"appId":   appId,
		"groupId": groupId,
	}

	if c, err := c.Find(nil, query); err != nil {
		log.Errorf("FindGroupUsers Error: %v", err)
		return nil, err
	} else {
		for c.Next(nil) {
			var user GroupUser
			if err := c.Decode(&user); err != nil {
				log.Errorf("Decode GroupUsers Error: %v", err)
				return nil, err
			}
			users = append(users, &user)
		}
	}

	return users, nil
}

func CreateGroupUsers(appId, groupId string, userIds []string) error {
	c := database.C(CollectionGroupUser)

	for _, userId := range userIds {
		user := GroupUser{
			AppId:   appId,
			GroupId: groupId,
			UserId:  userId,
			CTime:   time.Now(),
			UTime:   time.Now(),
		}

		if _, err := c.ReplaceOne(nil, bson.M{"appId": appId, "groupId": groupId, "userId": userId}, &user, options.Replace().SetUpsert(true)); err != nil {
			log.Errorf("CreateGroupUsers Error: %v", err)
		}
	}

	return nil
}

func DeleteGroupUsers(appId, groupId string, userIds []string) error {
	c := database.C(CollectionGroupUser)

	for _, userId := range userIds {
		query := bson.M{
			"appId":   appId,
			"groupId": groupId,
			"userId":  userId,
		}
		if _, err := c.DeleteMany(nil, query); err != nil {
			log.Errorf("DeleteGroupUsers Error: %v", err)
			return err
		}
	}
	return nil
}
