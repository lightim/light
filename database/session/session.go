package session

import (
	"time"

	"gitlab.com/lightim/light/database"
	"gitlab.com/lightim/light/log"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	CollectionName = "session"
)

/* 更新用户会话 */
func UpsertSession(appId, userId string, stype int, targetId string) (*Session, error) {
	c := database.C(CollectionName)

	session := Session{
		AppId:    appId,
		UserId:   userId,
		Type:     stype,
		TargetId: targetId,
		UTime:    time.Now(),
		CTime:    time.Now(),
	}

	query := bson.M{
		"appId":    appId,
		"userId":   userId,
		"type":     stype,
		"targetId": targetId,
	}

	if _, err := c.ReplaceOne(nil, query, &session, options.Replace().SetUpsert(true)); err != nil {
		log.Errorf("UpsertSession Error: %v", err)
		return nil, err
	}

	return &session, nil
}

/* 查询用户会话记录 */
func FindUserSessions(appId, userId string, n int) ([]*Session, error) {
	c := database.C(CollectionName)

	sessions := make([]*Session, 0)

	query := bson.M{
		"appId":  appId,
		"userId": userId,
	}

	if c, err := c.Find(nil, query, options.Find().SetSort(bson.M{"utime": -1}).SetLimit(int64(n))); err != nil {
		log.Errorf("FindUserSessions Error: %v", err)
		return nil, err
	} else {
		for c.Next(nil) {
			var sess Session
			if err := c.Decode(&sess); err != nil {
				log.Errorf("Decode Session Error: %v", err)
				return nil, err
			}
			sessions = append(sessions, &sess)
		}
	}
	return sessions, nil
}
