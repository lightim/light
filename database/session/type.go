package session

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

const (
	STypePerson = 1
	STypeGroup  = 2
	STypeRoom   = 3
)

type Session struct {
	Id       primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	AppId    string             `bson:"appId" json:"app_id"`
	UserId   string             `bson:"userId" json:"user_id"`
	Type     int                `bson:"type" json:"type"`
	TargetId string             `bson:"targetId" json:"target_id"`
	UTime    time.Time          `bson:"utime" json:"utime"`
	CTime    time.Time          `bson:"ctime" json:"ctime"`
}

func (*Session) TableName() string {
	return "session"
}
