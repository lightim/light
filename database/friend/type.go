package friend

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Friend struct {
	Id       primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	AppId    string             `bson:"appId" json:"app_id"`
	UserId   string             `bson:"userId" json:"user_id"`
	FriendId string             `bson:"friendId" json:"friend_id"`
	UTime    time.Time          `bson:"utime" json:"utime"`
	CTime    time.Time          `bson:"ctime" json:"ctime"`
}
