package friend

import (
	"time"

	"gitlab.com/lightim/light/database"
	"gitlab.com/lightim/light/log"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	CacheFriendPrefix = "light.friend"
)

const (
	CollectionFriend = "friend"
)

func CreateFriend(appId, userId, friendId string) error {
	c := database.C(CollectionFriend)

	if _, err := c.ReplaceOne(nil, bson.M{"appId": appId, "userId": userId, "friendId": friendId}, &Friend{
		AppId:    appId,
		UserId:   userId,
		FriendId: friendId,
		UTime:    time.Now(),
		CTime:    time.Now(),
	}, options.Replace().SetUpsert(true)); err != nil {
		log.Errorf("CreateFriend Error:%v", err)
		return err
	}

	if _, err := c.ReplaceOne(nil, bson.M{"appId": appId, "userId": friendId, "friendId": userId}, &Friend{
		AppId:    appId,
		UserId:   friendId,
		FriendId: userId,
		UTime:    time.Now(),
		CTime:    time.Now(),
	}, options.Replace().SetUpsert(true)); err != nil {
		log.Errorf("CreateFriend Error:%v", err)
		return err
	}

	return nil
}

func DeleteFriend(appId, userId, friendId string) error {
	c := database.C(CollectionFriend)

	selector := bson.M{
		"appId": appId,
		"$or": []bson.M{
			{
				"userId":   userId,
				"friendId": friendId,
			},
			{
				"userId":   friendId,
				"friendId": userId,
			},
		},
	}

	if _, err := c.DeleteMany(nil, selector); err != nil {
		log.Errorf("Delete Friend Error:%v", err)
		return err
	}

	return nil
}

func GetFriendByUFId(appId, userId, friendId string) (*Friend, error) {
	c := database.C(CollectionFriend)

	var friend Friend

	query := bson.M{
		"appId":    appId,
		"userId":   userId,
		"friendId": friendId,
	}

	if err := c.FindOne(nil, query).Decode(&friend); err != nil {
		if err != mongo.ErrNoDocuments {
			log.Errorf("GetFriendByUFId Error:%v", err)
			return nil, err
		}
		return nil, nil
	}

	return &friend, nil
}
