package user

import (
	"time"

	"gitlab.com/lightim/light/database"
	"gitlab.com/lightim/light/log"
	"gitlab.com/lightim/light/x"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

const (
	CacheAppPrefix = "light.app"

	CollectionApp = "app"
)

func GetAppById(appId string) (*App, error) {
	c := database.C(CollectionApp)

	var app App

	query := bson.M{
		"appId": appId,
	}
	if err := c.FindOne(nil, query).Decode(&app); err != nil {
		if err != mongo.ErrNoDocuments {
			log.Errorf("GetAppById Error: %v", err)
			return nil, err
		}
		return nil, nil
	}

	return &app, nil
}

func CreateApp(name string, allowAnonymous, allowStranger bool) (*App, error) {
	c := database.C(CollectionApp)

	app := App{
		AppId:          x.RandStringRunes(12),
		SecretKey:      x.UUID(),
		Name:           name,
		AllowAnonymous: allowAnonymous,
		AllowStranger:  allowStranger,
		CTime:          time.Now(),
		UTime:          time.Now(),
	}
	if _, err := c.InsertOne(nil, &app); err != nil {
		log.Errorf("CreateApp Error: %v", err)
		return nil, err
	}
	return &app, nil
}
