package user

import (
	"time"

	"gitlab.com/lightim/light/database"
	"gitlab.com/lightim/light/log"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	CollectionToken = "token"
)

func GetTokenWithClient(token, client string) (*Token, error) {
	c := database.C(CollectionToken)

	var t Token

	query := bson.M{
		"token":  token,
		"client": client,
	}

	if err := c.FindOne(nil, query).Decode(&t); err != nil {
		if err != mongo.ErrNoDocuments {
			log.Errorf("GetTokenWithClient Error: %v", err)
			return nil, err
		}
		return nil, nil
	}
	return &t, nil
}

func CreateToken(appId, userId, client, token string) (*Token, error) {
	c := database.C(CollectionToken)

	t := Token{
		AppId:  appId,
		UserId: userId,
		Client: client,
		Token:  token,
		CTime:  time.Now(),
		UTime:  time.Now(),
	}

	selector := bson.M{
		"appId":  appId,
		"userId": userId,
		"client": client,
	}

	if _, err := c.ReplaceOne(nil, selector, t, options.Replace().SetUpsert(true)); err != nil {
		log.Errorf("CreateToken Error: %v", err)
		return nil, err
	}

	return &t, nil
}
