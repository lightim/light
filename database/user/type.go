package user

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

const (
	SessionClientWeb     = 0
	SessionClientAndroid = 1
	SessionClientIOS     = 2
)

type App struct {
	Id             primitive.ObjectID `bson:"_id,omitempty" json:"-"`
	AppId          string             `bson:"appId" json:"app_id"`
	SecretKey      string             `bson:"secretKey" json:"secret_key"`
	Name           string             `bson:"name" json:"name"`
	AllowAnonymous bool               `bson:"allowAnonymous" json:"allow_anonymous"`
	AllowStranger  bool               `bson:"allowStranger" json:"allow_stranger"`
	UTime          time.Time          `bson:"utime" json:"utime"`
	CTime          time.Time          `bson:"ctime" json:"ctime"`
}

type Token struct {
	Id     primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	AppId  string             `bson:"appId" json:"app_id"`
	UserId string             `bson:"userId" json:"user_id"`
	Token  string             `bson:"token" json:"token"`
	Client string             `bson:"client" json:"client"`
	UTime  time.Time          `bson:"utime" json:"utime"`
	CTime  time.Time          `bson:"ctime" json:"ctime"`
}
