package cache

import (
	"encoding/json"
	"time"

	"gitlab.com/lightim/light/x"

	"github.com/go-redis/redis"
)

var _redisClient *redis.Client

func Init(addr, password string, db int) error {
	_redisClient = redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: password,
		DB:       db,
	})

	if _, err := _redisClient.Ping().Result(); err != nil {
		panic(err)
	}
	return nil
}

func Set(key string, v interface{}, t time.Duration) error {
	return _redisClient.Set(key, v, t).Err()
}

func SetJSON(key string, v interface{}, t time.Duration) error {
	data, err := json.Marshal(v)
	if err != nil {
		return err
	}
	return Set(key, data, t)
}

func Del(key ...string) error {
	return _redisClient.Del(key...).Err()
}

func Get(key string) (string, error) {
	return _redisClient.Get(key).Result()
}

func GetJSON(key string, v interface{}) bool {
	data, _ := Get(key)
	if data == "" {
		return false
	}
	if err := json.Unmarshal([]byte(data), v); err != nil {
		return false
	}
	return true
}

func Lock(key string, t time.Duration) (string, error) {
	v := x.UUID()
	lkey := "lock." + key
	for {
		ok, err := _redisClient.SetNX(lkey, v, t).Result()
		if err != nil {
			return "", err
		} else if ok {
			return v, nil
		}
		time.Sleep(time.Millisecond * 100)
	}
}

func Unlock(key, v string) error {
	lkey := "lock." + key
	return _redisClient.Eval(`if redis.call("get", KEYS[1]) == ARGV[1] then return redis.call("del", KEYS[1]) else return 0 end`, []string{lkey}, v).Err()
}
