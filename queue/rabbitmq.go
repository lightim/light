package queue

import (
	"github.com/streadway/amqp"
)

type Queue struct {
	QueueName    string
	ExchangeName string
	RouterKey    string

	Channel *amqp.Channel
}

var (
	_conn *amqp.Connection

	MessageQueue *Queue = nil
	EventQueue   *Queue = nil
	PushQueue    *Queue = nil
)

func NewQueue(conn *amqp.Connection, exchangeName, queueName, routerKey string) (*Queue, error) {
	ch, err := conn.Channel()
	if err != nil {
		return nil, err
	}

	if err := ch.ExchangeDeclare(exchangeName, amqp.ExchangeTopic, true, false, false, false, nil); err != nil {
		return nil, err
	}

	if q, err := ch.QueueDeclare(queueName, true, false, false, false, nil); err != nil {
		return nil, err
	} else if err := ch.QueueBind(q.Name, routerKey, exchangeName, false, nil); err != nil {
		return nil, err
	}

	if err := ch.Qos(2, 0, false); err != nil {
		return nil, err
	}
	return &Queue{
		QueueName:    queueName,
		ExchangeName: exchangeName,
		RouterKey:    routerKey,
		Channel:      ch,
	}, nil
}

func InitConn(mqurl string) error {
	var err error
	_conn, err = amqp.Dial(mqurl)
	if err != nil {
		panic(err)
	}

	return nil
}

func InitMessageQueue() error {
	var err error
	MessageQueue, err = NewQueue(_conn, "lightmex", "lightmq", "light.message")
	if err != nil {
		panic(err)
	}
	return nil
}

func InitEventQueue() error {
	var err error
	EventQueue, err = NewQueue(_conn, "lighteex", "lighteq", "light.event")
	if err != nil {
		panic(err)
	}
	return nil
}

func InitPushQueue() error {
	var err error
	PushQueue, err = NewQueue(_conn, "lightpex", "lightpq", "light.push")
	if err != nil {
		panic(err)
	}
	return nil
}
