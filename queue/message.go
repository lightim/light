package queue

import (
	"github.com/golang/protobuf/proto"
	"github.com/streadway/amqp"
	"gitlab.com/lightim/light/log"
)

func PubMessage(m proto.Message) {
	body, err := proto.Marshal(m)
	if err != nil {
		log.Errorf("Marshal error:%v", err)
		return
	}
	data := amqp.Publishing{
		DeliveryMode: amqp.Persistent,
		ContentType:  "application/octet-stream",
		Type:         "light/message",
		Body:         body,
	}
	err = MessageQueue.Channel.Publish(MessageQueue.ExchangeName, MessageQueue.RouterKey, false, false, data)
	if err != nil {
		log.Errorf("PubMessage error:%v", err)
	}
}
