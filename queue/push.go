package queue

import (
	"encoding/json"

	"github.com/golang/protobuf/proto"
	"github.com/streadway/amqp"
	"gitlab.com/lightim/light/log"
)

func PubNotification(m proto.Message) {
	body, err := json.Marshal(m)
	if err != nil {
		log.Errorf("Marshal error:%v", err)
		return
	}
	data := amqp.Publishing{
		DeliveryMode: amqp.Persistent,
		ContentType:  "application/json",
		Type:         "light/message",
		Body:         body,
	}
	err = PushQueue.Channel.Publish(PushQueue.ExchangeName, PushQueue.RouterKey, false, false, data)
	if err != nil {
		log.Errorf("PubNotification error:%v", err)
	}
}
