package proto

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"errors"
	"net"
	"sync"
	"time"

	"fmt"
	proto "github.com/golang/protobuf/proto"
	"github.com/gorilla/websocket"
)

var (
	ErrInvalidMessage = errors.New("invalid message")
)

type Conn interface {
	Read() (*Message, error)
	Write(*Message) error
	Close() error
	SetReadDeadline(time.Time) error
}

type TCPConn struct {
	net.Conn
	sync.Mutex
}

func NewTCPConn(c net.Conn) Conn {
	return &TCPConn{
		Conn: c,
	}
}

func (c *TCPConn) read(i uint32) ([]byte, error) {
	buf := make([]byte, i)

	readn := uint32(0)
	for readn < i {
		n, err := c.Conn.Read(buf[readn:])
		if err != nil {
			return nil, err
		}
		readn += uint32(n)
	}
	return buf, nil
}

func (c *TCPConn) readSize() (uint32, error) {
	sizebuf, err := c.read(4)
	if err != nil {
		return 0, err
	}

	var size uint32
	if err := binary.Read(bytes.NewReader(sizebuf), binary.LittleEndian, &size); err != nil {
		return 0, err
	}
	return size, nil
}

func (c *TCPConn) Read() (*Message, error) {
	size, err := c.readSize()
	if err != nil {
		return nil, err
	} else if size == 0 || size > 1024*1024*10 {
		return nil, ErrInvalidMessage
	}

	buf, err := c.read(size)
	if err != nil {
		return nil, err
	}

	var message Message
	if err := proto.Unmarshal(buf, &message); err != nil {
		return nil, err
	}
	return &message, nil
}

func (c *TCPConn) Write(m *Message) error {
	defer c.Unlock()
	c.Lock()
	data, err := proto.Marshal(m)
	if err != nil {
		return err
	}
	size := len(data)
	sizebuf := bytes.NewBuffer(nil)
	if err := binary.Write(sizebuf, binary.LittleEndian, uint32(size)); err != nil {
		return err
	}

	if _, err := c.Conn.Write(sizebuf.Bytes()); err != nil {
		return err
	} else if _, err := c.Conn.Write(data); err != nil {
		return err
	}
	return nil
}

func (c *TCPConn) WriteBytes(data []byte) error {
	defer c.Unlock()
	c.Lock()

	size := len(data)
	sizebuf := bytes.NewBuffer(nil)
	if err := binary.Write(sizebuf, binary.LittleEndian, uint32(size)); err != nil {
		return err
	}

	if _, err := c.Conn.Write(sizebuf.Bytes()); err != nil {
		return err
	} else if _, err := c.Conn.Write(data); err != nil {
		return err
	}
	return nil
}

type WSConn struct {
	*websocket.Conn
}

func NewWSConn(c *websocket.Conn) Conn {
	return &WSConn{
		Conn: c,
	}
}

func (c *WSConn) Read() (*Message, error) {
	_, data, err := c.Conn.ReadMessage()
	if err != nil {
		fmt.Printf("%v\n", err)
		return nil, err
	}
	var m Message
	if err := json.Unmarshal(data, &m); err != nil {
		fmt.Printf("%s!!!!\n", data)
		return nil, err
	}
	return &m, nil
}

func (c *WSConn) Write(m *Message) error {
	data, err := json.Marshal(m)
	if err != nil {
		return err
	}
	return c.Conn.WriteMessage(websocket.TextMessage, data)
}

func (c *WSConn) SetReadDeadline(t time.Time) error {
	return nil
}
