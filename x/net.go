package x

import (
	"net"
)

var _privateIPBlocks []*net.IPNet

func init() {
	for _, cidr := range []string{
		"10.0.0.0/8",     // RFC1918
		"172.16.0.0/12",  // RFC1918
		"192.168.0.0/16", // RFC1918
		"fe80::/10",      // IPv6 link-local
		"fc00::/7",       // IPv6 unique local addr
	} {
		_, block, err := net.ParseCIDR(cidr)
		if err != nil {
			panic(err)
		}
		_privateIPBlocks = append(_privateIPBlocks, block)
	}
}

/* 判断IP地址是否是内网IP */
func IsPrivateIP(ip net.IP) bool {
	for _, block := range _privateIPBlocks {
		if block.Contains(ip) {
			return true
		}
	}
	return false
}

/* 获取内网IP */
func FindLocalIPs() ([]string, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return nil, err
	}
	ips := make([]string, 0)
	for _, iface := range ifaces {
		if iface.Flags&net.FlagUp == 0 {
			continue // interface down
		}
		if iface.Flags&net.FlagLoopback != 0 {
			continue // loopback interface
		}
		addrs, err := iface.Addrs()
		if err != nil {
			return nil, err
		}
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			if ip == nil || ip.IsLoopback() || !IsPrivateIP(ip) {
				continue
			}
			ips = append(ips, ip.String())
		}
	}
	return ips, nil
}
